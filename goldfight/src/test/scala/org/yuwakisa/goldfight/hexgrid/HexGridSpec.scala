package org.yuwakisa.goldfight.hexgrid

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class HexGridSpec extends AnyFlatSpec with Matchers {

  private val grid = HexGrid(20, 10, 32)

  "pixXY" must "pick the correct X Y" in {
    grid.pixXY(0) must be (16, 16)
    grid.pixXY(20) mustBe (32, 48)
    grid.pixXY(1) mustBe (48, 16)
    grid.pixXY(19) mustBe (624,16)
    grid.pixXY(20 * 10 + 19) mustBe (624,336)
    grid.pixXY(20 * 10) mustBe (16,336)
  }

  "shifedRow" must "identify the correct shifted row" in {
    grid.shiftedRow(0) mustBe 0
    grid.shiftedRow(20) mustBe 1
  }
}
