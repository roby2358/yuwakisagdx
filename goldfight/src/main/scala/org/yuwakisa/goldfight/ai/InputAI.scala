package org.yuwakisa.goldfight.ai

import com.badlogic.gdx.{Gdx, Input, InputAdapter, InputProcessor}
import org.yuwakisa.goldfight.hexgrid.HexGrid
import org.yuwakisa.goldfight.{GameState, Move, Pass, Plan, ThingAt}

case object InputAI extends InputAdapter with KnightAI {

  lazy val hook: Unit = Gdx.input.setInputProcessor(this)

  var lastKey: Int = Input.Keys.UNKNOWN

  override def keyTyped(keycode: Char): Boolean = {
    lastKey = keycode
    super.keyDown(keycode)
  }

  def keyToAt(grid: HexGrid, at: Int): Option[Int] = {
    val shift = grid.shiftedRow(at) == 1
    val next = if (lastKey == 'q')
      at + grid.width + (if (!shift) -1 else 0)
    else if (lastKey == 'a')
      at - 1
    else if (lastKey == 'z')
      at - grid.width + (if (!shift) -1 else 0)
    else if (lastKey == 'e')
      at + grid.width + (if (shift) 1 else 0)
    else if (lastKey == 'd') // Input.Keys.D)
      at + 1
    else if (lastKey == 'c')
      at - grid.width + (if (shift) 1 else 0)
    else
      -1
    lastKey = Input.Keys.UNKNOWN

    Option(next).filter(grid.adjacent(at).contains(_))
  }

  def plan(gameState: GameState, ta: ThingAt): Plan = {
    hook
    keyToAt(gameState.grid, ta.at) match {
      case Some(mm) =>
        Move(ta.at, mm)
      case None =>
        Pass()
    }
  }
}
