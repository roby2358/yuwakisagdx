package org.yuwakisa.goldfight.ai

import org.yuwakisa.goldfight.{GameState, Move, Plan, ThingAt}

import scala.util.Random

case object RandomAI extends KnightAI {

  def move(gameState: GameState, at: Int) = {
    val adjacent = gameState.grid.adjacent(at)
    val next = adjacent(Random.nextInt(adjacent.length))
    Move(at, next)
  }

  def plan(gameState: GameState, ta: ThingAt): Plan = {
    move(gameState, ta.at)
  }
}
