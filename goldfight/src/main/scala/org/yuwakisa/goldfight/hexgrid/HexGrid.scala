package org.yuwakisa.goldfight.hexgrid

import scala.util.Random

case class HexGrid(width: Int, height: Int, pix: Int) {
  val haPix: Int = pix / 2
  val windowWidth: Int = pix * width + pix / 2 + haPix
  val windowHeight: Int = pix * height + haPix

  val lastAt: Int = height * width - 1

  val rr = new Random()

  def shiftedRow(at: Int): Int = (at / width) & 1

  def atXY(at: Int): (Int, Int, Int) = (at % width, at / width, shiftedRow(at))

  def atRow(at: Int): (Int, Int) = {
    val (_, y, _) = atXY(at)
    (y * width, y * width + width - 1)
  }

  def pixXY(at: Int): (Int, Int) = ((at % width) * pix + shiftedRow(at) * haPix + haPix, (at / width) * pix + haPix)

  def xyAt(x: Int, y: Int): Int = y * width + x

  def pixToAt(x: Int, y: Int): Int = {
    val yy = (windowHeight - y) / 32
    val haX = if ((yy & 1) == 1) haPix else 0
    val xx = (x - haX) / 32
    yy * width + xx
  }

  def randomCell: Int = rr.nextInt(width * height)

  def randomEdgeCell: Int = {
    val edge = rr.nextInt(4)

    val (x, y) = if (edge == 0)
      (0, rr.nextInt(height))
    else if (edge == 1)
      (rr.nextInt(width), 0)
    else if (edge == 2) {
      val y = rr.nextInt(height - 1)
      (width - 1 - (y & 1), y)
    } else
      (rr.nextInt(width), height - 1)

    xyAt(x, y)
  }

  def valid(at: Int): Boolean = at >= 0 && at <= lastAt

  def adjacent(at: Int): Seq[Int] = {
    val shift = shiftedRow(at)
    val (start, last) = atRow(at)
    val naShift = 1 - shift
    val left = if (at > start)
      Seq(at - 1,
        at - naShift - width,
        at - naShift + width)
    else
      Seq()

    val right = if (at < last)
      Seq(at + 1,
        at + shift - width,
        at + shift + width)
    else
      Seq()

    left.concat(right).filter(valid)
  }

  /**
   * Return a (consistent) sequence of cells at some range, with None for invalid cells
   *
   * @param at    the center
   * @param range the range
   * @return sequence of cells or none
   */
  def allAtRange(at: Int, range: Int): Seq[Option[Int]] = {
    val (x, y, _) = atXY(at)

    for (yy <- -range to range;
         rowY = y + yy;
         shift = y & 1;
         left = x + -range + (math.abs(yy) + shift) / 2;
         right = x + range - (math.abs(yy) + 1 - shift) / 2;
         rowX <- left to right;
         at = xyAt(rowX, rowY))
      yield {
        if (rowX >= 0 && rowX < width && valid(at)) Some(at) else None
      }
  }

  def distance(a: Int, b: Int): Int = {
    val (aX, aY, shift) = atXY(a)
    val (bX, bY, _) = atXY(b)
    val dx = math.abs(aX - bX)
    val dy = math.abs(aY - bY)
    val naShift = 1 - shift

    if (dx == 0 && dy == 0)
      0
    else if (aX < bX && dy < 2 * dx - shift)
      dx + (dy + naShift) / 2
    else if (aX > bX && dy < 2 * dx - naShift)
      dx + (dy + shift) / 2
    else
      dy
  }

  def nextStep(a: Int, b: Int): Int = {
    if (a == b) return b

    val (aX, aY) = pixXY(a)
    val (bX, bY) = pixXY(b)
    val dX = bX - aX
    val dY = bY - aY
    val d = math.sqrt(dX * dX + dY * dY)
    val nX = aX + dX * 32f / d + 0.5f
    val nY = aY + dY * 32f / d + 0.5f
    pixToAt(nX.toInt, windowHeight - nY.toInt)
  }
}
