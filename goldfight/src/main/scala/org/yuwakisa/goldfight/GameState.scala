package org.yuwakisa.goldfight

import org.yuwakisa.goldfight.hexgrid.HexGrid

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.Random

case class ThingAt(thing: Thing, at: Int)

case class GameState(grid: HexGrid, eventSink: Events.EventSink = new Events.EventSink {}) {

  val things: mutable.ArrayBuffer[ThingAt] = new mutable.ArrayBuffer[ThingAt]()

  var nextCycle: Long = 0L

  var cycleTime: Long = 500L

  def thingAt(at: Int): Option[ThingAt] = things.find(_.at == at)

  @tailrec
  final def empty(rr: => Int): Int = {
    val a = rr

    if (thingAt(a).isEmpty)
      a
    else
    // it has to find one eventually, right?
      empty(rr)
  }

  def add(thing: Thing, at: Int): Either[Option[ThingAt], GameState] = {
    val that = thingAt(at)
    if (that.isDefined) return Left(that)

    things.append(ThingAt(thing, at))
    Right(this)
  }

  def remove(at: Int): Either[Option[ThingAt], GameState] = {
    if (thingAt(at).isEmpty) return Left(None)

    things.filterInPlace(_.at != at)
    Right(this)
  }

  /**
   * Move a thing at one place to another
   *
   * @param from Location coming from
   * @param to   Location going to
   * @return Right with the gamestate if successful, or Left with the thing and location if not
   */
  def move(from: Int, to: Int): Either[Option[ThingAt], GameState] = {
    val thing = thingAt(from)
    if (thing.isEmpty) return Left(None)

    val that = thingAt(to)
    if (that.isDefined) return Left(that)

    things.filterInPlace(_.at != from)
      .append(ThingAt(thing.get.thing, to))
    Right(this)
  }

  /**
   * Put a new treasure on the grid
   */
  def replaceTreasure(): Unit = {
    val gold = (Random.nextDouble * Random.nextDouble * 1000).toInt
    add(Treasure(gold), grid.randomCell)
  }

  /**
   * Put a new knight with the same AI on the grid
   *
   * @param knight Knight to copy
   */
  def replaceKnight(knight: Knight): Unit = {
    add(Knight(knight.ai, 0), empty(grid.randomEdgeCell))
  }

  /**
   * Move the knight, adding the gold, and remove the treasure
   *
   * @param m        The move
   * @param knight   The knight
   * @param treasure The treasure
   */
  def knightTakesTreasure(m: Move, knight: Knight, treasure: Thing): Unit = {
    //
    things.filterInPlace(a => a.at != m.from && a.at != m.to)
      .append(ThingAt(knight.addGold(treasure.gold), m.to))

    eventSink.take(Events.TakeTreasure(m.to))

    replaceTreasure()
  }

  /**
   * Remove the vanquished knight and add its gold to the victor
   *
   * @param a       Location of victor
   * @param b       Location of vanquished
   * @param knightA Victor
   * @param knightB Vanquished
   */
  def knightVanquishesKnight(a: Int, b: Int, knightA: Knight, knightB: Knight): Unit = {
    things.filterInPlace(c => c.at != a && c.at != b)
      .append(ThingAt(knightA.addGold(knightB.gold), a))

    eventSink.take(Events.Vanquish(b))
    eventSink.take(Events.TakeTreasure(a))

    replaceKnight(knightB)
  }

  /**
   * Two knights class inconclusively
   *
   * @param a Knight A
   * @param b Knight B
   */
  def knightsClash(a: Int, b: Int): Unit = {
    eventSink.take(Events.Attack(a))
    eventSink.take(Events.Defend(b))
  }

  /**
   * A knight attacks another knight
   *
   * @param a       Location of the attacker
   * @param b       Location of the defender
   * @param knightA The attacker
   * @param knightB The defender
   */
  def knightAttacks(a: Int, b: Int, knightA: Knight, knightB: Knight): Unit = {
    val totes = knightA.strength * 2 + knightB.strength + 10
    val r = Random.nextInt(totes)
    if (r <= knightA.strength * 2)
      knightVanquishesKnight(a, b, knightA, knightB)
    else if (r >= totes - knightB.strength.toFloat)
      knightVanquishesKnight(b, a, knightB, knightA)
    else
      knightsClash(a, b)
  }

  /**
   * Go after some delay
   *
   * @return this
   */
  def timedGo(): GameState = {
    val now = System.currentTimeMillis()
    if (now > nextCycle) {
      nextCycle = now + cycleTime
      go()
    }
    else
      this
  }

  /**
   * Perform a cycle
   *
   * @return this
   */
  def go(): GameState = {

    // make plans
    val plans = things.collect { ta: ThingAt =>
      ta.thing match {
        case p: Planner => p.makePlan(this, ta)
      }
    }

    // shuffle plans
    Random.shuffle(plans)

    // execute plans
    plans.foreach {
      case m: Move =>
        (thingAt(m.from), move(m.from, m.to)) match {
          case (Some(ThingAt(knight: Knight, _)), Left(Some(ThingAt(treasure: Treasure, _)))) =>
            knightTakesTreasure(m, knight, treasure)
          case (Some(ThingAt(knightA: Knight, _)), Left(Some(ThingAt(knightB: Knight, _)))) =>
            knightAttacks(m.from, m.to, knightA, knightB)
          case _ =>
            ;
        }
      case _: Pass =>
        ;
      case unkn =>
        println("HUH? " + unkn.getClass.getSimpleName)
    }

    this
  }
}
