package org.yuwakisa.goldfight

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.TextureRegion

case class TextureGrid(source: String, w: Int, h: Int) {
  val texture = new Texture(source)
  val textures: Seq[TextureRegion] = Range(0, 35).map(chop)

  private def chop(i: Int) = {
    val xx = i % 5
    val yy = i / 5
    val x = xx * w
    val y = yy * h
    new TextureRegion(texture, x, y, w - 1, h - 1)
  }

  def apply(i: Int): TextureRegion = textures(i)

  def dispose(): Unit = texture.dispose()
}
