package org.yuwakisa.goldfight

import org.yuwakisa.goldfight.ai.KnightAI

case class Knight(ai: KnightAI, gold: Int) extends Planner {

  /**
   * Level scales up with gold
   * @return the knight's level
   */
  def level: Int = math.min(4, gold / 200)

  /**
   * @return image number determined by gold amount
   */
  def image: Int = 5 + level

  /**
   * @return strength determined by gold amount
   */
  def strength: Int = 3 + math.min(5, gold / 200)

  /**
   * @return lifeline determined by gold amount
   */
  def lifeline: Int = 200 * math.min(2, gold/ 500)

  /**
   * Right now the plan is to stay put
   *
   * @param gameState
   * @param ta
   * @return a plan
   */
  def makePlan(gameState: GameState, ta: ThingAt): Plan = ai.plan(gameState, ta)

  /**
   * @param g gold to add
   * @return an instance of a Knight with the extra gold
   */
  def addGold(g: Int): Knight = Knight(ai, gold + g)
}
