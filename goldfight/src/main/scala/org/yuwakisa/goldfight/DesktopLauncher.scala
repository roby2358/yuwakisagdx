package org.yuwakisa.goldfight

import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}

object DesktopLauncher {
  def main(arg: Array[String]): Unit = {
    val game = new org.yuwakisa.goldfight.GoldFight
    new LwjglApplication(game, game.config)
  }
}