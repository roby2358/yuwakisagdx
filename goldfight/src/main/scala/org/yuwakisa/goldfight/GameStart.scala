package org.yuwakisa.goldfight

import org.yuwakisa.goldfight.ai.{InputAI, KnightAI, RandomAI, SimpleAI}
import org.yuwakisa.goldfight.hexgrid.HexGrid

import scala.util.Random

case class GameStart(eventSink: Events.EventSink) {

  private val knightCount = Map[String, Int](
    //    "player" -> 1,
    "random" -> 2,
    "simple" -> 4)

  private val maxGold = 1000
  private val treasureCount = 6
  private val grid = HexGrid(32, 20, 32)
  private val gameState = GameState(grid, eventSink)

  def addNKnights(ai: KnightAI, n: Int): Unit = for (_ <- 1 to n)
    gameState.add(Knight(ai, 0), gameState.empty(grid.randomCell))

  def addKnights(): Unit =
    knightCount.foreach {
      case (k, n) if k == "player" => addNKnights(InputAI, n)
      case (k, n) if k == "random" => addNKnights(RandomAI, n)
      case (k, n) if k == "simple" => addNKnights(SimpleAI, n)
    }

  def randomGold: Int = (Random.nextDouble * Random.nextDouble * Random.nextDouble * maxGold).toInt

  def addTreasure(): Unit =
    for (_ <- 1 to treasureCount)
      gameState.add(Treasure(randomGold), gameState.empty(grid.randomCell))

  def begin(): GameState = {
    addKnights()
    addTreasure()
    gameState
  }
}
