package org.yuwakisa.goldfight

/**
 * Defines all the actions we can plan
 */
sealed trait Plan

/**
 * Take 1 step in a direction (or none)
 *
 * @param from Where moving from
 * @param to Where moving to
 */
case class Move(from: Int, to: Int) extends Plan

/**
 * Don't do anything
 */
case class Pass() extends Plan

/**
 * A planner is a thing that can plan
 */
trait Planner extends Thing {
  /**
   * Make a plan, given the state of the game and a position
   *
   * @param gameState The current gamestate
   * @param ta The thing and at
   * @return a plan
   */
  def makePlan(gameState: GameState, ta: ThingAt): Plan
}
