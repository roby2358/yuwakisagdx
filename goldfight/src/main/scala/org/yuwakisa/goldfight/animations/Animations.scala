package org.yuwakisa.goldfight.animations

import org.yuwakisa.goldfight.Events

import scala.collection.mutable

/**
 * One step in an animation
 *
 * @param image the image to show
 * @param time  how long to show it
 */
case class AnimationStep(image: Int, time: Long)

/**
 * A model for actual animations
 *
 * @param steps Steps in the animation
 */
case class Animation(steps: Seq[AnimationStep]) {

  def start(at: Int, now: Long) = AnimationAt(at, steps, 0, now + steps.head.time)
}

/**
 * A realization of an animation
 *
 * @param at      Location in the grid
 * @param steps   Animation steps
 * @param current Current step index
 * @param next    Time to switch to next step
 */
case class AnimationAt(at: Int, steps: Seq[AnimationStep], current: Int, next: Long) {

  def active: Boolean = current < steps.length

  def image: Int = steps(current).image

  def advance(elapsed: Long): AnimationAt = AnimationAt(at, steps, current + 1, next + steps(current).time - elapsed)
}

/**
 * Manage the state of animations
 */
case class Animations() extends Events.EventSink {

  var animations: mutable.ArrayBuffer[AnimationAt] = mutable.ArrayBuffer()

  /**
   * Add an event's animation to the animation queue
   *
   * @param event the event
   */
  override def take(event: Events.Event): Unit = {
    animations.synchronized {
      val now = System.currentTimeMillis
      animations.append(event.animation.start(event.at, now))
      event.sound.foreach(_.play())
    }
  }

  /**
   * Cycle the animations, advancing the ones that are ready, and dropping
   * the ones that are done
   *
   * @return this
   */
  def cycle(): Animations =
    this.synchronized {
      if (animations.isEmpty) return this

      val now = System.currentTimeMillis
      animations = animations.collect { case a if a.active =>
        if (now >= a.next)
          a.advance(now - a.next)
        else
          a
      }
        .filter(_.active)

      this
    }

  /**
   * Apply the function to each animation
   *
   * @param f Function for each animation
   */
  def foreach(f: AnimationAt => Unit): Unit =
    this.synchronized(animations.foreach(f(_)))
}