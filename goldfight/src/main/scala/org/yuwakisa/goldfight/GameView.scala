package org.yuwakisa.goldfight

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.{Color, GL20}
import org.yuwakisa.goldfight.animations.Animations
import org.yuwakisa.goldfight.hexgrid.HexGrid

trait Disposable {
  def dispose(): Unit
}

object GameView {
  val Colors: Seq[Color] = Seq(Color.RED, Color.ORANGE, Color.YELLOW, Color.GREEN, Color.BLUE, Color.NAVY, Color.VIOLET)

  val Grayscale: Seq[Color] = (1 to 8).map { n =>
    val c: Float = n.toFloat / 10f
    new Color().set(c, c, c, 1.0f)
  }
}

case class GameView(gameState: GameState, animations: Animations) extends Events.EventSink {

  val grid: HexGrid = gameState.grid

  val textureGridName: String = "gold1sm.png"

  lazy val batch = new SpriteBatch with Disposable
  lazy val shapeRenderer = new ShapeRenderer() with Disposable
  lazy val img = new TextureGrid(textureGridName, grid.pix, grid.pix) with Disposable
  private lazy val toDispose = Seq[Disposable](batch, shapeRenderer, img)

  /**
   * Build graphics configuration
   *
   * @return
   */
  def config: LwjglApplicationConfiguration =
    (new LwjglApplicationConfiguration).tapWith({ cfg =>
      cfg.width = grid.windowWidth
      cfg.height = grid.windowHeight
    })

  /**
   * Create the view
   */
  def create(): Unit = {
    println(Gdx.graphics.getDisplayMode.width, Gdx.graphics.getDisplayMode.height)

    //    Gdx.graphics.setWindowedMode(grid.windowWidth, grid.windowHeight)
  }

  /**
   * Prepare to render!
   */
  def prepRender(): Unit = {
    Gdx.gl.glClearColor(0f, .3f, .1f, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    Gdx.gl.glEnable(GL20.GL_BLEND)
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
    Gdx.gl.glDisable(GL20.GL_BLEND)
  }

  /**
   * Show all the things
   */
  def showThings(): Unit = {
    gameState.things.foreach { ta =>
      val (x, y) = grid.pixXY(ta.at)
      batch.draw(img(ta.thing.image), x, y)
    }
  }

  /**
   * Render all the animation images
   */
  def showAnimations(): Unit =
    animations.cycle().foreach { a =>
      val (x, y) = grid.pixXY(a.at)
      batch.draw(img(a.image), x, y)
    }

  //    val helper = ViewHelpers(this)

  def render(): Unit = {
    prepRender()

    batch.begin()
    showThings()
    showAnimations()

    //            val mouseAt = grid.pixToAt(Gdx.input.getX(), Gdx.input.getY() + grid.haPix)
    //    showRow(mouseAt)
    //    helper.showCells(grid.adjacent(mouseAt))
    //        val atRange = grid.allAtRange(mouseAt, 7)
    //        helper.showCells(atRange.flatten)
    //    showDistances(mouseAt)
    //    showNextStep(mouseAt)
    //        helper.showMouseCell(mouseAt)

    batch.end()
  }

  def dispose(): Unit = toDispose.foreach(_.dispose())
}
