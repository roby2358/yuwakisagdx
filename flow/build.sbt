val scala3Version = "3.0.0-M3"

lazy val root = project
  .in(file("."))
  .settings(
    name := "flow",
    organization := "org.yuwakisa",
    version := "0.1.0",

    scalaVersion := scala3Version,

    scalacOptions ++= Seq(
      "-language:implicitConversions",
      "-new-syntax",
      "-indent",
      "-Yindent-colons"
      //      "-rewrite"
    ),

    resolvers ++= Seq(
      "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
      "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
    ),

    libraryDependencies ++= Seq(
      "com.badlogicgames.gdx" % "gdx" % "1.9.11",
      "com.badlogicgames.gdx" % "gdx-box2d" % "1.9.11",
      "com.badlogicgames.gdx" % "gdx-controllers" % "1.9.11",

      "com.badlogicgames.gdx" % "gdx-backend-lwjgl" % "1.9.11",

      "com.badlogicgames.gdx" % "gdx-platform" % "1.9.11" classifier "natives-desktop",
      "com.badlogicgames.gdx" % "gdx-box2d-platform" % "1.9.11" classifier "natives-desktop",
      "com.badlogicgames.gdx" % "gdx-bullet-platform" % "1.9.11" classifier "natives-desktop",
      "com.badlogicgames.gdx" % "gdx-freetype-platform" % "1.9.11" classifier "natives-desktop",
      "com.badlogicgames.gdx" % "gdx-controllers-platform" % "1.9.11" classifier "natives-desktop",

      "com.novocode" % "junit-interface" % "0.11" % "test"
    ) ++ Seq(
      "org.json4s" %% "json4s-native" % "3.6.11",
      "org.json4s" %% "json4s-jackson" % "3.6.11",
      "org.scala-lang.modules" %% "scala-parallel-collections" % "0.2.0",
      "org.scalanlp" %% "breeze" % "1.1",
      "org.scalanlp" %% "breeze-natives" % "1.1",
      "org.scalanlp" %% "breeze-viz" % "1.1",
      "org.scala-graph" %% "graph-core" % "1.13.2",
      "org.scalactic" %% "scalactic" % "3.2.0",
      "org.scalatest" %% "scalatest" % "3.2.2" % "test",
    )
      .map(_.withDottyCompat(scalaVersion.value))
  )
  
