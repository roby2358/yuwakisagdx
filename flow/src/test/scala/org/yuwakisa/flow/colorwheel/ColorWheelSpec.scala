package org.yuwakisa.flow.colorwheel

import org.junit.Assert.assertEquals
import org.junit.Test
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class ColorWheelSpec extends AnyFlatSpec with Matchers :

  @Test def hslToRgb(): Unit =
    val rgb0 = ColorWheel.hslToRgb(0, 0, 0)
    assertEquals((0, 0, 0), rgb0)

    val rgb1 = ColorWheel.hslToRgb(0, 0, 1)
    assertEquals((1, 1, 1), rgb1)

    val rgb2 = ColorWheel.hslToRgb(0, 1, 0.5)
    assertEquals((1, 0, 0), rgb2)

    val rgb3 = ColorWheel.hslToRgb(.33333333333333333333333, 1, 0.5)
    assertEquals((0, 1, 0), rgb3)

    val rgb4 = ColorWheel.hslToRgb(.66666666666666666666666, 1, 0.5)
    assertEquals((0, 0, 1), rgb4)

    val rgb5 = ColorWheel.hslToRgb(0.8333333333333333333333, 1, 0.5)
    assertEquals((1, 0, 1), rgb5)

    val rgb6 = ColorWheel.hslToRgb(0.5, 1, 0.25)
    assertEquals((0, 0.5, 0.5), rgb6)

    val rgb7 = ColorWheel.hslToRgb(0.8333333333333333333333, 1, 0.25)
    assertEquals((0.5, 0, 0.5), rgb7)

  @Test def rgbToHsl(): Unit =
    val hsl0 = ColorWheel.rgbToHsl(0, 0, 0)
    assertEquals((0, 0, 0), hsl0)

    val hsl1 = ColorWheel.rgbToHsl(1, 1, 1)
    assertEquals((0, 0, 1), hsl1)

    val hsl2 = ColorWheel.rgbToHsl(1, 0, 0)
    assertEquals((0, 1, 0.5), hsl2)

    val hsl3 = ColorWheel.rgbToHsl(0, 1, 0)
    assertEquals((0.33333333333333333333333, 1, 0.5), hsl3)

    val hsl4 = ColorWheel.rgbToHsl(0, 0, 1)
    assertEquals((0.6666666666666666, 1, 0.5), hsl4)

    val hsl6 = ColorWheel.rgbToHsl(0.5, 0.5, 0)
    assertEquals((0.16666666666666666, 1, 0.25), hsl6)

    val hsl5 = ColorWheel.rgbToHsl(0.5, 0, 0.5)
    assertEquals((0.8333333333333334, 1, 0.25), hsl5)

    val hsl7 = ColorWheel.rgbToHsl(0, 0.5, 0.5)
    assertEquals((0.5, 1, 0.25), hsl7)
