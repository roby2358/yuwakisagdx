package org.yuwakisa.flow.model

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class LineSegmentSpec extends AnyFlatSpec with Matchers {

  val l0 = LineSegment(Point(-1, -1), Point(1, 1))
  val l1 = LineSegment(Point(-1, 1), Point(1, -1))
  val l2 = LineSegment(Point(-1, -1), Point(-2, 1))
  val l3 = LineSegment(Point(-2, -3), Point(1, 1))
  val l4 = LineSegment(Point(2, 2), Point(3, 3))
  val l5 = LineSegment(Point(-1, 0), Point(1, 2))
  val l6 = LineSegment(Point(0, 0), Point(2, 2))

//  "intersect" must "find intersection at 0 0" in {
//    val p = l0.intersect(l1)
//    val q = l1.intersect(l0)
//
//    p.get must equal(Point(0, 0))
//    p must equal(q)
//  }
//
//  it must "find intersection at end" in {
//    val p = l0.intersect(l2)
//    val q = l2.intersect(l0)
//
//    p.get must equal(l0.a)
//    p must equal(q)
//  }
//
//  it must "find intersection at other end" in {
//    val p = l0.intersect(l3)
//    val q = l3.intersect(l0)
//
//    p.get must equal(l0.a)
//    p must equal(q)
//  }
//
//  it must "find no intersection" in {
//    val p = l0.intersect(l4)
//    val q = l4.intersect(l0)
//
//    p.get must equal(None)
//    p must equal(q)
//  }
//
//  it must "find also no intersection" in {
//    val p = l0.intersect(l5)
//    val q = l5.intersect(l0)
//
//    p.get must equal(None)
//    p must equal(q)
//  }
//
//  it must "find also no intersection in overlap" in {
//    val p = l0.intersect(l6)
//    val q = l6.intersect(l0)
//
//    p.get must equal(None)
//    p must equal(q)
//  }
}
