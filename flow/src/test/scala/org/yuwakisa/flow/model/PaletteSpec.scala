package org.yuwakisa.flow.colorwheel

import org.junit.Assert.assertEquals
import org.junit.Test
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class PaletteSpec extends AnyFlatSpec with Matchers :

  @Test def ofHex(): Unit =
    val actual = Palette.ofHex("80")
    assertEquals(0.5d, actual, 0d)

  @Test def ofHtml(): Unit =
    val actual = Palette.ofHtml("#4080C0", "4080C0", "c0c0c0")
    assertEquals(Seq((0.25,0.5,0.75), (0.25,0.5,0.75), (0.75,0.75,0.75)), actual)

  @Test def ofN(): Unit =
    val actual = Palette.ofN(16, (8, 8, 8))
    assertEquals(Seq((0.5,0.5,0.5)), actual)
