package org.yuwakisa.flow

object Stuff:

  implicit class Doer[A](a: A):

    def tap[B](f: A => B): A =
      f(a)
      a

    def chain[B](f: A => B): B = f(a)

  object Conversions:
    implicit def d2i(x: Double): Int = x.toInt

    implicit def d2f(x: Double): Int = x.toInt

    implicit def f2i(x: Double): Int = x.toInt

  def sqr(n: Double): Double = n * n
