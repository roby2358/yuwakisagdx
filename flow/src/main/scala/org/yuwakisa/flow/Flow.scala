package org.yuwakisa.flow

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import org.yuwakisa.flow.state._
import org.yuwakisa.flow.Stuff._

object Flow :
  def time[A](f: => A) =
    val start = System.nanoTime
    f.tap :
      _ =>
        println(f"Time: ${(System.nanoTime - start) / 1000d / 1000000d}%.3f micros")

class Flow extends ApplicationAdapter:
  val claz = GravityNoise
  val state: GameState = Flow.time { claz.begin() }
  val view: GameView = GameView(state)

  def config: LwjglApplicationConfiguration = view.config

  override def create(): Unit = view.create()

  override def render(): Unit =
    state.next()
    view.render()

  override def dispose(): Unit = view.dispose()
