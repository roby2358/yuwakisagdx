package org.yuwakisa.flow

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import org.yuwakisa.flow.colorwheel.ColorWheel.Rgb
import org.yuwakisa.flow.colorwheel.Colors
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.stuff.ScreenCap

trait GameState:

  val height: Int

  val width: Int
  
  val palette: Palette

  lazy val colors = Colors(10000, palette)

  def next(): Unit = { }

  def render(batch: SpriteBatch): Unit

  def screencap(): Unit = ScreenCap().write()
