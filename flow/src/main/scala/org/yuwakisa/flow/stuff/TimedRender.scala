package org.yuwakisa.flow.stuff

import com.badlogic.gdx.graphics.g2d.SpriteBatch

class TimedRender:

  def now: Long = System.currentTimeMillis

  var last: Long = 0

  def render(iter: Long, f: SpriteBatch => Unit): SpriteBatch => Unit =
    { s =>
      if last + iter > now then
        last += iter * 2
        f(s)
    }
