package org.yuwakisa.flow.stuff

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.{Pixmap, PixmapIO}
import com.badlogic.gdx.utils.ScreenUtils
import org.yuwakisa.flow.GameView.dateFormatter

import java.io.File
import java.time.Instant

class ScreenCap :
  
  val name = "gen" + dateFormatter.format(Instant.now())

  def capture(): Pixmap =
    val pixmap = new Pixmap(Gdx.graphics.getWidth, Gdx.graphics.getHeight, Format.RGBA8888)
    val pixels = pixmap.getPixels
    pixels.clear
    pixels.put(ScreenUtils.getFrameBufferPixels(true))
    pixmap
  
  def write(): Unit =
    val pixmap = capture()
    write(pixmap)
    pixmap.dispose()
  
  def write(pixmap: Pixmap): Unit =
    val file = Gdx.files.local(name + ".png")
    PixmapIO.writePNG(file, pixmap)

    val d = new File(System.getProperty("user.dir"))
    println(System.getProperty("user.dir"))
    println(d.listFiles.filter(_.isFile).toList.mkString("\n"))
