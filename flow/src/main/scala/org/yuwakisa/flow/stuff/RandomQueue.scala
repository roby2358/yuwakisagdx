package org.yuwakisa.flow.stuff

import scala.collection.mutable
import scala.util.Random

case class RandomQueue[T]() extends mutable.Queue[T]:

  override def dequeue(): T = remove(Random.nextInt(length))
