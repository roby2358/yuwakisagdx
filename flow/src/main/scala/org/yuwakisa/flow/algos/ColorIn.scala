package org.yuwakisa.flow.algos

import com.badlogic.gdx.graphics.Color
import org.yuwakisa.flow.algos.PriorityBubbleWalker.PriorityValue
import org.yuwakisa.flow.model.Circle
import scalax.collection.{Graph, GraphEdge}

import scala.collection.mutable
import scala.util.Random

class ColorIn(val graph: Graph[Circle[PriorityValue], GraphEdge.UnDiEdge],
              colors: IndexedSeq[Color]):

  val allColors = (0 until colors.length).toSet
  
  val colored: mutable.Map[Int, Int] =
    mutable.Map((0 until colors.length).map((_, 0)):_*)

  def reset(c: Circle[PriorityValue]): Unit = { c.color = -1 } 
    
  def visit(n: graph.NodeT): Unit =
    // Guard: quit if already colored
    if n.value.asInstanceOf[Circle[PriorityValue]].color > -1d then return
    
    val adjacent = n.neighbors

    val taken = adjacent
      .map(_.value.asInstanceOf[Circle[PriorityValue]].color.toInt)
      .filter(_ != -1d)
    
    val available = allColors -- taken
    
    val newvalue =
      if available.isEmpty then
        Random.nextInt(colors.length)
      else
        colored.toSeq
          .sortBy(_._2 + Random.nextDouble())
          .map(_._1.toInt)
          .find(available.contains)
          .getOrElse(Random.nextInt(colors.length))

    colored.update(newvalue, colored(newvalue) + 1)

    n.value.asInstanceOf[Circle[PriorityValue]].color = newvalue

  def run(): Unit =

    graph.nodes.foreach :
      c => reset(c)

    graph
      .innerNodeTraverser(graph.nodes.last)
      .foreach(visit)

    println(colored)
