package org.yuwakisa.flow.algos

import org.yuwakisa.flow.functions._
import org.yuwakisa.flow.model._
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph

import scala.collection.mutable
import scala.util.Random

object PriorityBubbleWalker:

  implicit class Functionstate(f: () => Functionstate)
    extends Function0[Functionstate]:
    def apply(): Functionstate = f()

  object Priority :
    def lowest(c: Circle[PriorityValue], d: Circle[PriorityValue]): Double =
      (d.value.get.priority - c.value.get.priority) * 1000
  
    def highest(c: Circle[PriorityValue], d: Circle[PriorityValue]): Double =
      -lowest(c, d)

    val order: (Circle[PriorityValue], Circle[PriorityValue]) => Double = lowest
  
    def distance(center: Point)(c: Circle[_]): Double =
      c.distance(center) + Random.nextDouble()
  
    def misses(c: Circle[PriorityValue]): Double =
      c.value.get.misses + Random.nextDouble()
  
    def random(c: Circle[_]): Double =
      Random.nextDouble()

    val priority: (c: Circle[PriorityValue]) => Double = random

  case class PriorityValue(var priority: Double) :
    var value: Double = 0d
    var misses: Int = 0

    def set(v: Double, m: Int): PriorityValue =
      value = v
      misses = m
      this

  implicit class PriorityCircle(c: Circle[PriorityValue]) extends
    Ordered[Circle[PriorityValue]]:
    def compare(that: Circle[PriorityValue]): Int =
      Priority.order(c, that).toInt


case class PriorityBubbleWalker(width: Int,
                                height: Int,
                                desiredNum: Int,
                                stopNum: Int,
                                maxMisses: Int,
                                terrain: Option[Layer]):

  import PriorityBubbleWalker._

  object NewValue :
    def smallerWithMisses(c: Circle[PriorityValue]): Double =
      (c.value.get.misses.toDouble / maxMisses) * (maxR - minR) + minR
  
    var newRadius: Circle[PriorityValue] => Double = smallerWithMisses
  
    def gaussianFromPrev(c: Circle[PriorityValue]): Double =
      Cell.minMax(c.value.get.value * (1 + Random.nextGaussian() / 12d))
  
    var f: Circle[PriorityValue] => Double = gaussianFromPrev

  val boundary: Rectangle = Rectangle(0, 0, width, height)
  val center: Point = Point(width / 2, height / 2)

//  val prioritize: Circle[PriorityValue] => Double = distancePriority(
//    Point(width / 2, height / 2))
  val prioritize: Circle[PriorityValue] => Double = Priority.priority

  var hits: Int = 0
  var misses: Int = 0

  def sqr(x: Double): Double = x * x

  val graph: Graph[Circle[PriorityValue], UnDiEdge] = Graph()
  val queue: mutable.PriorityQueue[Circle[PriorityValue]] =
    new mutable.PriorityQueue[Circle[PriorityValue]]()

  lazy val nodes: IndexedSeq[Circle[PriorityValue]] =
    graph.nodes.toOuter.toIndexedSeq

  val maxR: Double = math.max(math.sqrt((width * height * .9 * 1.5) /
    (desiredNum * math.Pi) * 2), 10)
  val minR: Double = math.max(maxR / 4, 3)

  def rr: Double = Cell.minMax(Random.nextGaussian() / 6 + .5) *
    (maxR - minR) + minR

  println((minR, maxR))
  println("desired" + width * height * .9)
  println("expected" + math.Pi * maxR * maxR * desiredNum)
  println("expected mean" + math.Pi * sqr((maxR + minR) / 2) * desiredNum)

  object Start :
    def random =
      val x1: Double = Random.nextInt(width - 2 * r1.toInt) + r1
      val y1: Double = Random.nextInt(height - 2 * r1.toInt) + r1
      (x1, y1)
  
    def center =
      (width / 2, height / 2)
      
  val r1: Double = maxR / 2
  val (x1, y1) = Start.center
  val v1: PriorityValue = PriorityValue(0).set(Random.nextDouble(), maxMisses)
  val first: Circle[PriorityValue] = Circle(x1, y1, r1, v1)

  graph += first
  first.value.get.priority = prioritize(first)
  queue += first
  hits += 1

  def isValid(c: Circle[_]): Boolean = c.within(boundary) &&
    graph.nodes.forall(!_.collide(c))

  def accept(c: Circle[PriorityValue], d: Circle[PriorityValue]): Unit =
    hits += 1

    graph += d
    graph += d ~ c

    d.value.get.priority = prioritize(d)
    queue += d

    c.value.get.misses -= 6
    c.value.get.priority = prioritize(c)
    queue += c

  def retry(c: Circle[PriorityValue]): Unit =
    misses += 1

    c.value.get.misses -= 1
    if c.value.get.misses > 0 then
      c.value.get.priority = prioritize(c)
      queue += c
    if queue.nonEmpty then visit(queue.dequeue())

  def trace(c: Circle[_], vx: Double, vy: Double): Double =
    val points = Bresenham(c.center,
      Point(c.x + vx * (c.r + maxR),
        c.y + vy * (c.r + maxR)))
      .line
      .takeWhile({ p =>
        p.x >= 0 && p.y >= 0 && p.x <
          width && p.y < height
      })
    var tot = maxR
    var i = c.r.toInt
    while i < points.length && tot > 0 do
      val p = points(i)
      tot -= terrain.get(p.x, p.y).value * 3
      i += 1
  
    //      val r0 = math.max(minR, pi - c.r)
    //      val rs = (((r0 - minR) / (maxR - minR)) * 3).toInt
    //      rs / 3d * (maxR - minR) + minR
  
    val r = i - c.r
    if r < minR * 2 then {
      // less than 2 small circles, return the min (touching)
      minR
    } else if r < minR * 3 then {
      // cut off at 3 small circles
      minR * 2
    } else {
      // Use a big circle
      maxR
    }

  def visit(c: Circle[PriorityValue]): Unit =
    if c.value.get.misses < 0 then return

    val (vx, vy): (Double, Double) = Cell.valueToVector(Random.nextDouble())

    val r = if terrain.isEmpty then
      NewValue.f(c)
    else
      trace(c, vx, vy)

    val v = NewValue.f(c)
    val d = Circle(c.x + vx * (r + c.r),
      c.y + vy * (r + c.r),
      r,
      PriorityValue(0).set(v, maxMisses))

    if isValid(d) then
      accept(c, d)
    else
      retry(c)

  val done: Functionstate = { () => done }

  lazy val finish: Unit =
    val total = graph.nodes.map[Double](_.r).sum
    println("done! " + nodes.size)
    println("hits " + hits)
    println("misses " + misses)
    println("average r " + total / nodes.size)

    val area = graph.nodes.map({ c => math.Pi * sqr(c.r) }).sum
    println(("area %", area, width * height))

  val go: Functionstate = { () =>
    if graph.nodes.toOuter.size >= stopNum then
      queue.clear()
      finish
      done
    else if queue.isEmpty then
      finish
      done
    else
      visit(queue.dequeue())
      go
  }

  var next: Functionstate = go

  def complete(): PriorityBubbleWalker =
    while next != done do
      next = next()
    this
