package org.yuwakisa.flow

import com.badlogic.gdx.graphics.{Pixmap, Texture}
import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.utils.Disposable
import org.yuwakisa.flow.model.Layer
import org.yuwakisa.flow.Stuff._
import org.yuwakisa.flow.colorwheel.Colors

/**
 * Note: Declare pixmapper as lazy val because there's an implicit dependency
 *
 * @param width Width of the pixmap
 * @param height Height of the pixmap
 */
case class Pixmapper(width: Int, height: Int) extends Disposable:

  val pixmap: Pixmap = new Pixmap(width, height, Format.RGB888)
  lazy val theText: Texture = new Texture(pixmap)
  var todo: Option[Pixmap] = Some(pixmap)

  def draw(batch: Batch)(f: Pixmap => Unit): Pixmapper =
    f(pixmap)

    val text = new Texture(pixmap)
    batch.begin()
    batch.draw(text, 0, 0)
    batch.end()
    text.dispose()

    this

  def drawOnce(batch: Batch)(f: Pixmap => Unit): Pixmapper =
    todo.foreach(f(_))
    todo = None

    batch.begin()
    batch.draw(theText, 0, 0)
    batch.end()

    this

  def drawLayer(layer: Layer, pix: Int, colors: Colors): Pixmapper =
    val plot: (Int, Int) => Unit =
      if pix == 1 then
        pixmap.drawPixel
      else
        pixmap.fillRectangle(_, _, pix, pix)

    layer.foreach :
      (x, y) =>
        pixmap.setColor(colors(layer(x, y)))
        plot(x * pix, y * pix)

    this

  def copyPixmap: Pixmap =
    Pixmap(width, height, Format.RGB888).tap :
      p => p.drawPixmap(pixmap, 0, 0)

  def dispose(): Unit = pixmap.dispose()
