package org.yuwakisa.flow

import com.badlogic.gdx.backends.lwjgl.LwjglApplication

object DesktopLauncher :

  def main(arg: Array[String]): Unit =
    val game = new org.yuwakisa.flow.Flow
    new LwjglApplication(game, game.config)
