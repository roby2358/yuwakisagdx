package org.yuwakisa.flow.model

object Circle:

  def apply[A](x: Double, y: Double, r: Double, a: A): Circle[A] =
    Circle[A](Point(x, y), r).take(a)

case class Circle[A](center: Point, r: Double):

  var id: Int = 0
  var color: Double = 0d
  var value: Option[A] = None

  val x: Double = center.x
  val y: Double = center.y

  def take(a: A): Circle[A] =
    value = Some(a)
    this
    
  def v: A =
    value.get

  def distance(that: Point): Double = center.distance(that)

  def distance(that: Circle[_]): Double = distance(that.center)

  def collide(that: Circle[_], tolerance: Double = 1): Boolean =
    r + that.r - distance(that.center) > tolerance

  def touching(that: Circle[_], tolerance: Double = 1): Boolean =
    math.abs(distance(that.center) - r - that.r) < tolerance

  def within(rr: Rectangle): Boolean =
    (center.x - r > rr.x0) &&
    (center.x + r < rr.x1) &&
    (center.y - r > rr.y0) &&
    (center.y + r < rr.y1)
