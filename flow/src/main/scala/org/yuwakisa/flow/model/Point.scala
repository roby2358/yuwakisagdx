package org.yuwakisa.flow.model

import breeze.linalg.DenseMatrix

object Point:

  def apply(m: DenseMatrix[Double]): Point =
    Point(m(0, 0).toInt, m(1, 0).toInt)

case class Point(x: Double, y: Double):

  def vector(that: Point): (Double, Double) =
    (that.x - x, that.y - y)

  def matrix: DenseMatrix[Double] =
    DenseMatrix(x.toDouble, y.toDouble, 1d)

  def distance(that: Point): Double =
    val (dx, dy) = vector(that)
    math.sqrt(dx * dx + dy * dy)

  def diff(that: Point): Double =
    math.max(math.abs(that.x - x), math.abs(that.y - y))

  def unitVector(that: Point): (Double, Double) =
    val d = distance(that)
    val (vx, vy) = vector(that)
    (vx / d, vy / d)

  def mid(that: Point): Point = Point((that.x + x) / 2, (that.y + y) / 2)
