package org.yuwakisa.flow.model

import breeze.linalg.DenseMatrix

import scala.annotation.tailrec
import scala.util.Random

object PackedCircles:
  val minR: Int = 5
  val maxR: Int = 100

//  def rr: Int = Random.nextInt((maxR - minR) / 5) * 5 + minR
  def rr: Int = ((Random.nextGaussian() / 6 + .5) / 4 *
    (maxR - minR) / 5).toInt * 5 + minR

case class PackedCircles(var r1: Double):

  import PackedCircles._

  var r2: Double = rr
  var centers: DenseMatrix[Double] = DenseMatrix(0d, r1 + r2)
  var radii: Seq[Double] = Seq(r2)

  def sqr(x: Double): Double = x * x

  // Math
  // x^2 + y^2 = (r1 + r3)^2
  // x^2 = r1^2 + 2r1r3 + r3^2 - y^2
  //
  // x^2 + (r1 + r2 - y)^2 = (r2 + r3)^2
  // x^2 + r1^2 + 2r1r2 - 2r1y + r2^2 - 2r2y + y^2 = r2^2 + 2r2r3 + r3^2
  //
  // (r1^2 + 2r1r3 + r3^2 - y^2) + r1^2 + 2r1r2 - 2r1y + r2^2 - 2r2y + y^2 -
  // r2^2 - 2r2r3 - r3^2 = 0
  //  - 2r1y - 2r2y + 2r1^2 + 2r1r2 + 2r1r3 - 2r2r3 = 0
  //  r1y + r2y - r1^2 - r1r2 - r1r3 + r2r3 = 0

  def findThird(r3: Int): Circle[Double] =
    val y = (sqr(r1) + r1 * r2 + r1 * r3 - r2 * r3) / (r1 + r2)
    val x = math.sqrt(sqr(r1 + r3) - sqr(y))
    Circle(x, y, r3, 0d)

  def fillGap(c3: Circle[Double]): Option[Circle[Double]] =
    // math: https://math.stackexchange.com/questions/3100828/calculate-the-circle-that-touches-three-other-circles#3290944

    val r2 = radii.last
    val y2 = r1 + r2

    val Ka = sqr(r1) - sqr(r2) + sqr(y2)
    val Kb = sqr(r1) - sqr(c3.r) + sqr(c3.x) + sqr(c3.y)
    val D = c3.x * -y2

    if D == 0 then return None

    val A0 = (Ka * -c3.y + Kb * y2) / (2 * D)
    val B0 = (Ka * -c3.x) / (2 * D)
    val A1 = (r1 * (y2 - c3.y) + r2 * c3.y + c3.r * -y2) / D
    val B1 = (r1 * -c3.x + r2 * c3.x) / D
    val C0 = sqr(A0) + sqr(B0) - sqr(r1)
    val C1 = A1 * A0 + B1 * B0 - r1
    val C2 = sqr(A1) + sqr(B1) - 1

    if C2 == 0 then return None

    val r = (-C1 - math.sqrt(sqr(C1) - C0 * C2)) / C2

    if r.isNaN || r < minR || r > maxR then return None

    val x = A0 + A1 * r
    val y = B0 + B1 * r

    Some(Circle(-x, y, math.floor(r), 0d))

  def add(c: Circle[Double]): Unit =
    val add = DenseMatrix(c.x, c.y)
    centers = DenseMatrix.horzcat(centers, add)
    radii = radii :+ c.r

  def nth(i: Int): Circle[Double] =
    val first = centers(::, i).toArray
    Circle(first(0), first(1), radii(i), 0d)

  def circles: Seq[Circle[Double]] = (0 until centers.cols).map(nth)

  def rotate(theta: Double): DenseMatrix[Double] =
    DenseMatrix((math.cos(theta), math.sin(theta)),
      (-math.sin(theta), math.cos(theta)))

  def spin(theta: Double): PackedCircles =
    centers = rotate(theta) * centers
    this

  def makeCircles(): PackedCircles =
    makeOne()
    this

  @tailrec
  final def makeOne(): Unit =
    val first = nth(0)
    val r3 = Random.nextDouble() * (maxR - minR) + minR
    val c3 = findThird(r3.toInt)

    if c3.collide(first) then
      fillGap(first).foreach(add)
    else
      add(c3)

      val theta: Double = math.acos(c3.y / (r1 + c3.r))
      spin(-theta)
      r2 = c3.r

      makeOne()
