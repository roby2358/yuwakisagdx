package org.yuwakisa.flow.model

case class LineSegment(a: Point, b: Point):

  lazy val length: Double = a.distance(b)

  def intersect(that: LineSegment): Option[Point] =
    val v = Point(b.x - a.x, b.y - a.y)
    val w = Point(that.b.x - that.a.x, that.b.y - that.a.y)

    val dd = -w.x * v.y + v.x * w.y

    if dd == 0 then return None

    val s = (-v.y * (a.x - that.a.x) + v.x * (a.y - that.a.y)) / dd
    val t = (w.x * (a.y - that.a.y) - w.y * (a.x - that.a.x)) / dd

    if s < 0 || s > 1 || t < 0 || t > 1 then
      None
    else
      val x = a.x + (t * v.x)
      val y = a.y + (t * v.y)
      Some(Point(x, y))

  /**
   * Find the point on the segment that's closest to the given point
   * @param p the point
   * @return the closest point
   */
  def closest(p: Point): Point =
    if p == a then return a
    if p == b then return b
    
    val A = p.x - a.x
    val B = p.y - a.y
    val C = b.x - a.x
    val D = b.y - a.y

    val dot = A * C + B * D
    val len_sq = C * C + D * D
    val param = if len_sq == 0 then -1d else dot / len_sq

    if param <= 0d then
      a
    else if param >= 1d then
      b
    else
      Point(a.x + param * C, a.y + param * D)
    
  // distance from a point to a line segment
  // https://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
  def distance(p: Point): Double = p.distance(closest(p))
