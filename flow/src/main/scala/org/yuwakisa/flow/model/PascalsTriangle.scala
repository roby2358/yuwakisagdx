package org.yuwakisa.flow.model

case class PascalsTriangle(n: Int):

  val triangle: Seq[Seq[Int]] = (2 until n).foldLeft(Seq(Seq(1), Seq(1, 1)))({ case (tri, _) =>
    val r = tri.last.sliding(2).map(_.sum).toList
    tri ++ Seq(List(1) ++ r ++ List(1))
  })

  def show(): Unit = println(triangle.mkString("\n"))
