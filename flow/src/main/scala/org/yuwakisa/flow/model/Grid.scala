package org.yuwakisa.flow.model

import scala.collection.parallel.CollectionConverters._

object Grid:

  def zeros(w: Int, h: Int): Grid =
    Grid(w, h,
      Layer.zero(w, h),
      Layer.zero(w, h),
      Layer.zero(w, h))

  def random(w: Int, h: Int): Grid =
    Grid(w, h,
      Layer.random(w, h),
      Layer.random(w, h),
      Layer.random(w, h))

case class Grid(width: Int,
                height: Int,
                var values: Layer,
                var vx: Layer,
                var vy: Layer):

  def apply(x: Int, y: Int): Float = values(x, y)

  def lastX: Int = width - 1

  def lastY: Int = height - 1

  def set(x: Int, y: Int, v: Float): Unit =
    values.cells(y)(x) = v

  def <<(f: Grid => Grid): Grid = f(this)

  def newLayers(f: (Int, Int) => (Float, Float, Float)): Grid =
    val newValues = Layer.zero(width, height)
    val newVx = Layer.zero(width, height)
    val newVy = Layer.zero(width, height)

    (0 until height * width).par.foreach({ i =>
      val x = i % width
      val y = i / width
      val (v, vx, vy) = f(x, y)
      newValues.cells(y)(x) = v
      newVx.cells(y)(x) = vx
      newVy.cells(y)(x) = vy
    })
    new Grid(width, height, newValues, newVx, newVy)

  def newValues(f: (Int, Int) => Float): Grid =
    copy(values = values.newValues(f))

  def add(that: Grid): Grid =
    values = values.newValues({ (x, y) =>
      (that.values(x, y) + this.values(x, y)) / 2f
    })
    this

  //    def weightedAddValues(those: Seq[Grid], weights: Seq[Int]): Grid = {
  //      val newCells = mutable.ArrayBuffer.tabulate(height, width)({ case (y, x) =>
  //        val a = those.zip(weights).map({ case (g, w) =>
  //          g(x, y).value * w
  //        }).sum / weights.sum
  //        Cell(a, 0f, 0f)
  //      })
  //      new Grid(width, height, newCells)
  //    }
