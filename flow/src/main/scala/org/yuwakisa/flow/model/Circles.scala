package org.yuwakisa.flow.model

import scala.collection.mutable
import scala.util.Random

case class Circles(width: Int, height: Int):

  val circles: mutable.ArrayBuffer[Circle[Double]] = mutable.ArrayBuffer()

  val minRadius = 10

  val maxRadius = 30

  def randomPoint(): Point =
    val x = Random.nextInt(width - 2 * maxRadius) + maxRadius
    val y = Random.nextInt(height - 2 * maxRadius) + maxRadius
    Point(x, y)

  def randomRadius: Double = Random.nextInt(maxRadius - minRadius) + minRadius

  val primes = Seq(11013017, 28422013, 93321341, 199909991, 664388783)

  def iterPoint(n: Int): (Int, Point) =
    val p = (n + primes(2)) % (width * height)
    (p, Point(p % width, p / width))

  def closest(p: Point): Option[(Double, Circle[Double])] =
    if circles.isEmpty then
      None
    else
      Some(circles.map({ c => (c.distance(p).toInt - c.r, c) })
        .minBy(_._1))

  def placeOne(p: Point): Option[Circle[Double]] =
    closest(p) match
      case Some((d, e)) if d >= minRadius && d <= maxRadius =>
        Some(Circle[Double](p, d).take(0))
      case Some((d, _)) if d > maxRadius =>
        Some(Circle[Double](p, randomRadius).take(0))
      case _ =>
        //        Some(Circle(p.x, p.y, Random.nextInt(maxRadius - minRadius) + minRadius))
        None

  def addSome(n: Int): Circles =
    val p = randomPoint()
    circles.append(Circle[Double](p, randomRadius).take(0))

    (0 until 10000).foreach { _ =>
      val p = randomPoint()
      placeOne(p).foreach(circles.append)
    }
    this
