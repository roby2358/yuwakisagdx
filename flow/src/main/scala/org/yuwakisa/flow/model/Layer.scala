package org.yuwakisa.flow.model

import scala.collection.mutable
import scala.collection.parallel.CollectionConverters._
import scala.util.Random

object Layer:

  type Cells = mutable.ArrayBuffer[mutable.ArrayBuffer[Float]]

  val BigPrime = 3187813

  def zero(width: Int, height: Int): Layer =
    build(width, height) {
      0f
    }

  def random(width: Int, height: Int): Layer =
    build(width, height) {
      Random.nextFloat()
    }

  def build(width: Int, height: Int)(f: => Float): Layer =
    val layer = Layer(width, height)
    layer.cells = mutable.ArrayBuffer.fill(height, width)(f)
    layer

case class Layer(width: Int, height: Int) {

  var cells: Layer.Cells = mutable.ArrayBuffer.fill(height, width)(0f)

  def xy(i: Int): (Int, Int) = (i % width, i / width)

  def lastX: Int = width - 1

  def lastY: Int = height - 1

  def apply(x: Double, y: Double): Float = cells(y.toInt)(x.toInt)

  def apply(p: Point): Float = this(p.x, p.y)

  def value(i: Int): Float = cells(i / width)(i % width)

  def set(x: Int, y: Int, v: Float): Unit =
    cells(y)(x) = v

  def add(x: Int, y: Int, v: Float): Unit =
    cells(y)(x) += v

  def set(p: Point, v: Float): Unit = set(p.x.toInt, p.y.toInt, v)

  def <<(f: Layer => Layer): Layer = f(this)

  def minMax: (Float, Float) =
    (0 until height * width).par.foldLeft((1f, -1f))({ case ((min, max), i) =>
      val v = value(i)
      (math.min(min, v), math.max(max, v))
    })

  def foreach(f: (Int, Int) => Unit): Unit =
    (0 until height * width).foreach({ i => f(i % width, i / width) })

  def foreachPrimey(f: (Int, Int) => Unit): Unit =
    var ii = 0
    (0 until height * width).foreach({ _ =>
      f(ii % width, ii / width)
      ii = (ii + Layer.BigPrime) % (height * width)
    })

  def newValues(f: (Int, Int) => Float): Layer =
    val newV = Layer.zero(width, height)

    (0 until height * width).par.foreach({ i =>
      val (x, y) = xy(i)
      newV.cells(y)(x) = f(x, y)
    })
    newV

  def add(that: Layer): Layer =
    newValues({ (x, y) => (that(x, y) + this (x, y)) / 2f })
}