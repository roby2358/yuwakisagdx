package org.yuwakisa.flow.model

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import org.yuwakisa.flow.Stuff._
import org.yuwakisa.flow.colorwheel.Colors
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.{GameState, Pixmapper}

case class StaticGrid(layer: Layer,
                 pix: Int,
                 palette: Palette) extends GameState:

  val height: Int = layer.height * pix

  val width: Int = layer.width * pix

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)
    .tap(_.drawLayer(layer, pix, colors))

  override def render(batch: SpriteBatch): Unit =
    pixmapper.drawOnce(batch) { _ => }
