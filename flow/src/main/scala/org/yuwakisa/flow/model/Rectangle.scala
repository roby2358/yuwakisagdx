package org.yuwakisa.flow.model

case class Rectangle(x0: Double, y0: Double, x1: Double, y1: Double)
