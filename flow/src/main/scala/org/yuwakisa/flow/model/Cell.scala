package org.yuwakisa.flow.model

import scala.util.Random

object Cell:

  val Pi = 3.141592653589793238d

  val Zero: Cell = Cell(0f, 0f, 0f)

  def valueToVector(v: Double): (Double, Double) =
    val a = (v * 2d - 1.0) * Pi
    (math.cos(a), math.sin(a))

  def random(): Cell =
    val r = Random.nextDouble()
    val (x, y) = valueToVector(Random.nextDouble())
    Cell(Random.nextFloat(),
      (x * r).toFloat,
      (y * r).toFloat)

  /**
   * Force the value within bounds
   * @param v the value to compare
   * @return value above min and below max
   */
  def minMax(v: Double): Double = math.min(1d, math.max(0d, v))

  def circ(v: Double): Double = v - math.floor(v)

  def bounce(v: Double): Double =
    if v > 1d then 2d - v
    else if v < 0 then -v
    else v

  def sin(v: Double): Double = math.sin((v * 2d - 1d) * math.Pi)

  def sigmoid(v: Double): Double = 1d / (1d + math.pow(math.E, -4.5 * (v -
    0.5)))

//  def sigmoid(v: Double): Double = v / math.sqrt(1d + v * v)

/**
 * @param value Value of the cell
 * @param vx    Vector x
 * @param vy    Vector y
 */
case class Cell(value: Float, vx: Float, vy: Float)
