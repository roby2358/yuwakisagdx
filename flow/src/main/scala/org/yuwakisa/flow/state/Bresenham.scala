package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.FPSLogger
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import org.yuwakisa.flow.colorwheel.ColorWheel.Rgb
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model.Point
import org.yuwakisa.flow.{GameState, Pixmapper, functions}

import scala.collection.mutable
import scala.util.Random

object Bresenham:
  def begin(): GameState = Bresenham(800, 800, Palette.random())

case class Bresenham(width: Int, height: Int, palette: Palette)
  extends GameState:

  val pixmapper: Pixmapper = Pixmapper(width, height)

  def now: Long = System.currentTimeMillis

  var last: Long = 0
  var rads: Double = 0
  var d: Int = 300
  var x: Int = width / 2
  var y: Int = height / 2
  var points: Seq[Point] = Seq()
  var color = .5

  lazy val fpsLogger = new FPSLogger()

  override def next(): Unit =
    rads += 2 * math.Pi / (60 * 60)
    color = math.min(math.max(color + Random.nextGaussian() / 120, 0), 1)

  def render(batch: SpriteBatch): Unit =
    fpsLogger.log()
    pixmapper.draw(batch)({ pixmap =>
      pixmap.setColor(colors(color))
      val points = functions.Bresenham.ray(Point(x, y), rads, d).line
      points.foreach({ p => pixmap.drawPixel(p.x.toInt, p.y.toInt) })
    })
