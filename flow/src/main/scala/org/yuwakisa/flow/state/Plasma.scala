package org.yuwakisa.flow.state

import org.yuwakisa.flow.colorwheel.{Colors, Luminance, Palette}
import org.yuwakisa.flow.functions.{Identity, Stretch}
import org.yuwakisa.flow.model.{Layer, PascalsTriangle, StaticGrid}
import org.yuwakisa.flow.{GameState, functions}

object Plasma:

  val width = 800
  val height = 800
  val pix = 1

  val pascals: Seq[Seq[Int]] = PascalsTriangle(35).triangle

  def begin(): GameState =
    StaticGrid(
      Layer.zero(width, height) <<
        functions.Plasma.plasmaIsland(8d) <<
        Stretch.histhilo(1000) <<
        Stretch.invertedS <<
//        functions.Blur(pascals(4)).gaussian <<
        //        Blur(pascals(30)).apply <<
        //        VCycle.apply <<
        //        Contor.apply <<
        Identity.apply,
      pix,
      Luminance.linear(0.1, 0.5)(Palette.random())
    )
