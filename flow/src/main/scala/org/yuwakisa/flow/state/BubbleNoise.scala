package org.yuwakisa.flow.state

import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.functions.{Identity, Stretch}
import org.yuwakisa.flow.model.{Layer, StaticGrid}
import org.yuwakisa.flow.{GameState, functions}

object BubbleNoise :

  val width = 200
  val height = 200
  val pix = 4

  def begin(): GameState =
    StaticGrid(
      Layer.zero(width, height) <<
      functions.BubbleNoise.apply <<
      Stretch.hilo <<
      Identity.apply,
      pix,
      Palette.random())
