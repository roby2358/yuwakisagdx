package org.yuwakisa.flow.state

import breeze.linalg._
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Disposable
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model.PackedCircles
import org.yuwakisa.flow.{GameState, Pixmapper}

import scala.util.Random

object PackedCircle:
  def begin(): GameState = PackedCircle(800, 800, Palette.random())

case class PackedCircle(width: Int, height: Int, palette: Palette)
  extends GameState with Disposable:

  val offX: Int = width / 2
  val offY: Int = height / 2
  val pixmapper: Pixmapper = Pixmapper(width, height)

  val minR = 10
  val maxR = offX / 2
  def rr = Random.nextDouble() * (maxR - minR) + minR

  var circles: PackedCircles = PackedCircles(rr).makeCircles()

  def now: Long = System.currentTimeMillis
  var last: Long = 0

  override def next(): Unit =
    if now > last + 3000 then
      last = now

      circles = PackedCircles(rr).makeCircles()

  var angle = 0d

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      pixmap.setColor(Color.BLACK)
      pixmap.fill()

      pixmap.setColor(Color.WHITE)
      pixmap.drawCircle(offX, offY, circles.r1.toInt)

      val all = circles.rotate(angle) * circles.centers
      angle += math.Pi / 600

      var i = 0
      all(::, *).foreach({ r =>
        val rr = r.toArray
        pixmap.drawCircle(offX + rr(0).toInt,
          offY - rr(1).toInt,
          circles.radii(i).toInt)
        i += 1
      })
    }

  def dispose(): Unit = pixmapper.dispose()
