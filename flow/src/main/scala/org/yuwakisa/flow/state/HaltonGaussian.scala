package org.yuwakisa.flow.state

import org.yuwakisa.flow.GameState
import org.yuwakisa.flow.algos.Halton
import org.yuwakisa.flow.colorwheel.{Luminance, Palette}
import org.yuwakisa.flow.functions.{Identity, Stretch}
import org.yuwakisa.flow.model.{Layer, StaticGrid}

import scala.util.Random

object HaltonGaussian :

  val width = 400
  val height = 400
  val pix = 1
  
  def begin(): GameState =
    var layer = Layer.zero(width, height)
    
    val a = Halton.random()
    val b = Halton.random()
    
    (0 until 1000).foreach :
      _ =>
        val x = (a.next() * width).toInt
        val y = (b.next() * height).toInt
        println((x, y))
        layer.set(x, y, Random.nextFloat())

    layer = layer <<
//      Stretch.hilo <<
//      Stretch.histhilo(10000) <<
      //      Stretch.invertedS <<
      Identity.apply

    val colors = Luminance.linear(0.1, 0.5)(Palette.random())

    StaticGrid(layer, pix, colors)
