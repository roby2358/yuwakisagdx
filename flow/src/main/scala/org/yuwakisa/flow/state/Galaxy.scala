package org.yuwakisa.flow.state

import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.functions.{Blur, Identity, Stretch, Swirl}
import org.yuwakisa.flow.model.{Layer, PascalsTriangle, StaticGrid}
import org.yuwakisa.flow.{GameState, functions}

object Galaxy:

  val width = 800
  val height = 800
  val pix = 1

  val pascals: Seq[Seq[Int]] = PascalsTriangle(35).triangle

  def begin(): GameState =
    StaticGrid(
      Layer.zero(width, height) <<
        functions.Plasma.plasma(4d) <<
        Stretch.histhilo(1000) <<
        Swirl.swirl(math.Pi / 2) <<
        Blur(8).gaussian <<
        //        functions.Blur(pascals(4)).gaussian <<
        //        Blur(pascals(30)).apply <<
        //        VCycle.apply <<
        //        Contor.apply <<
        Identity.apply,
      pix,
      Palette.random())
