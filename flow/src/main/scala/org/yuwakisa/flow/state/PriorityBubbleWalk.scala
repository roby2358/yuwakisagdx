package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{FPSLogger, Pixmap}
import org.yuwakisa.flow.algos.PriorityBubbleWalker.PriorityValue
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model._
import org.yuwakisa.flow.{GameState, Pixmapper, algos}

object PriorityBubbleWalk:
  val width = 800
  val height = 800

  val desiredNum = 36
  val stopNum = 10000
  val maxMisses = 30

  def begin(): GameState = PriorityBubbleWalk(width, height, Palette.random())

case class PriorityBubbleWalk(width: Int, height: Int, palette: Palette) extends GameState:

  import PriorityBubbleWalk._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  val walker: algos.PriorityBubbleWalker = algos.PriorityBubbleWalker(
    width = width,
    height = height,
    desiredNum = desiredNum,
    stopNum = stopNum,
    maxMisses = maxMisses,
    None)

  override def next(): Unit =
    walker.next()

  def circle(pixmap: Pixmap)(c: Circle[PriorityValue]): Unit =
    pixmap.setColor(colors(c.value.get.value))
    pixmap.drawCircle(c.x.toInt, c.y.toInt, c.r.toInt)

  val fpsLogger = new FPSLogger()

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      fpsLogger.log()

      pixmap.setColor(colors(.1f))
      pixmap.fill()

      walker.graph.nodes.toOuter
        .foreach(circle(pixmap))

      pixmap.setColor(colors(0.2f))
      walker.graph.edges.foreach { e =>
        pixmap.drawLine(e.head.x.toInt, e.head.y.toInt,
          e.last.x.toInt, e.last.y.toInt)
      }
    }
