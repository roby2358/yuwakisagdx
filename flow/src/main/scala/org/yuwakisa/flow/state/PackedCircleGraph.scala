package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Disposable
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model._
import org.yuwakisa.flow.stuff.RandomQueue
import org.yuwakisa.flow.{GameState, Pixmapper}
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph

import scala.util.Random

object PackedCircleGraph:
  def begin(): GameState = PackedCircleGraph(800, 800, Palette.random())

case class PackedCircleGraph(width: Int, height: Int, palette: Palette)
  extends GameState with Disposable:

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  val boundary: Rectangle = Rectangle(0, 0, width, height)

  val queue: RandomQueue[Circle[Double]] = RandomQueue()

  val graph: Graph[Circle[Double], UnDiEdge] = Graph()

  val r1: Double = PackedCircles.rr
  val x1: Double = Random.nextInt(width - 2 * r1.toInt) + r1
  val y1: Double = Random.nextInt(height - 2 * r1.toInt) + r1
  val first: Circle[Double] = Circle(x1, y1, r1, Random.nextDouble())

  graph += first
  queue += first

  def isValid(c: Circle[Double]): Boolean = c.within(boundary) &&
    graph.nodes.forall(!_.collide(c))

  def visit(c: Circle[Double]): Unit =
    val theta: Double = Random.nextDouble() * 2 * math.Pi - math.Pi
    val newOnes = PackedCircles(c.r)
      .makeCircles()
      .spin(theta)
      .circles
      .map { d =>
        Circle(d.x + c.x, d.y + c.y, d.r,
          Cell.minMax(c.v + Random.nextGaussian() / 9))
      }
      .filter(isValid)

    if newOnes.nonEmpty then
      queue ++= Random.shuffle(newOnes)
      val toCenter = newOnes.map(c ~ _)
      val toNext = (newOnes :+ newOnes.head).sliding(2)
        .collect({ case Seq(a, b) if a.touching(b) => a ~ b })
      (toCenter ++ toNext).foreach(graph.+=)

  lazy val finish: Unit = {
    //    graph.nodes.foreach({ n =>
    //      if (n.inDegree < 3)
    //        n.value.value = 0d
    //    })
  }

  override def next(): Unit =
    if queue.nonEmpty then
      visit(queue.dequeue())
    else
      finish

  object Draw:
    def plain(pixmap: Pixmap)(c: Circle[Double]): Unit =
      pixmap.setColor(colors(c.v.toFloat))
      pixmap.fillCircle(c.x.toInt, c.y.toInt, c.r.toInt)

    def circle(pixmap: Pixmap)(c: Circle[Double]): Unit =
      pixmap.setColor(colors(c.v.toFloat))
      pixmap.drawCircle(c.x.toInt, c.y.toInt, c.r.toInt)

    def darkCircles(pixmap: Pixmap)(c: Circle[Double]): Unit =
      pixmap.setColor(colors(0f))
      pixmap.drawCircle(c.x.toInt, c.y.toInt, c.r.toInt)

    def bright(pixmap: Pixmap)(c: Circle[Double]): Unit =
      pixmap.setColor(colors(1f))
      pixmap.fillCircle(c.x.toInt, c.y.toInt, c.r.toInt)

    def overlapping(pixmap: Pixmap)(c: Circle[Double]): Unit =
      pixmap.setColor(colors(c.v.toFloat * .717f))
      pixmap.fillCircle(c.x.toInt, c.y.toInt, (c.r * 1.5f).toInt)

      pixmap.setColor(colors(c.v.toFloat * .9f))
      pixmap.fillCircle(c.x.toInt, c.y.toInt, c.r.toInt)

      pixmap.setColor(colors(c.v.toFloat))
      pixmap.fillCircle(c.x.toInt, c.y.toInt, (c.r * .5f).toInt)

    def xx: Int = Random.nextInt(11) - 5

    def jiggled(pixmap: Pixmap)(c: Circle[Double]): Unit =
      pixmap.setColor(colors(c.v.toFloat * .717f))
      pixmap.fillCircle(c.x.toInt + xx, c.y.toInt + xx, (c.r * 1.5f).toInt)

      pixmap.setColor(colors(c.v.toFloat * .9f))
      pixmap.fillCircle(c.x.toInt + xx, c.y.toInt + xx, c.r.toInt)

      pixmap.setColor(colors(c.v.toFloat))
      pixmap.fillCircle(c.x.toInt + xx, c.y.toInt + xx, (c.r * .5f).toInt)

    def shine(pixmap: Pixmap)(c: Circle[Double]): Unit =
      pixmap.setColor(colors(c.v.toFloat * .717f))
      pixmap.fillCircle(c.x.toInt + 4, c.y.toInt - 4, c.r.toInt)

      pixmap.setColor(colors(c.v.toFloat * .8f))
      pixmap.fillCircle(c.x.toInt + 2, c.y.toInt - 2, (c.r * .8).toInt)

      pixmap.setColor(colors(c.v.toFloat))
      pixmap.fillCircle(c.x.toInt, c.y.toInt, (c.r * .5f).toInt)

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      pixmap.setColor(colors(.5f))
      pixmap.fill()

      graph.nodes.toOuter
        .toArray
        .sortWith((c, d) => c.r < d.r)
        .foreach(Draw.circle(pixmap))

      pixmap.setColor(colors(0.2f))
      graph.edges.foreach { e =>
        pixmap.drawLine(e._1.x.toInt, e._1.y.toInt,
          e._2.x.toInt, e._2.y.toInt)
      }
    }

  def dispose(): Unit = pixmapper.dispose()
