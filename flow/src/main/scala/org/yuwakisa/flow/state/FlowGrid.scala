package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{Pixmap, Texture}
import org.yuwakisa.flow.GameState
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.functions.{Flow, Identity, Plasma, Splatter, ValueToVector, ZeroValues}
import org.yuwakisa.flow.model.{Grid, PascalsTriangle}

object FlowGrid:
  val width = 200
  val height = 200
  val pix = 4

  val pascals: Seq[Seq[Int]] = PascalsTriangle(35).triangle

  def begin(): GameState =
    val g = Grid.zeros(width, height) <<
      Plasma.grid <<
      //      BubbleGrid.apply <<
      //      Stretch.apply <<
      //      Blur(pascals(16)).apply <<
      ValueToVector.apply <<
      ZeroValues.apply <<
      Splatter(20).apply <<
      Identity.apply
    //      PlasmaGrid.apply
    new FlowGrid(g, pix, Palette.random())

class FlowGrid(var grid: Grid,
               pix: Int,
               val palette: Palette)
  extends GameState:

  override def next(): Unit =
    grid = grid <<
      Splatter(10).apply <<
      Flow.apply <<
      Identity.apply

  val height: Int = grid.height * pix

  val width: Int = grid.width * pix

  lazy val pixmap = new Pixmap(width, height, Format.RGB888)

  val plot: (Int, Int) => Unit =
    if pix == 1 then pixmap.drawPixel
    else pixmap.fillRectangle(_, _, pix, pix)

  override def render(batch: SpriteBatch): Unit =
    grid.values.foreach({ (x, y) =>
      pixmap.setColor(colors(grid.values(x, y)))
      plot(x * pix, y * pix)
    })

    val text = new Texture(pixmap)
    batch.begin()
    batch.draw(text, 0, 0)
    batch.end()
    text.dispose()

  def dispose(): Unit =
    pixmap.dispose()
