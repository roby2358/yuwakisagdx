package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import org.yuwakisa.flow.algos.PriorityBubbleWalker.PriorityValue
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.functions.LinkGraph
import org.yuwakisa.flow.model.{Circle, LineSegment}
import org.yuwakisa.flow.{GameState, Pixmapper, algos}
import scalax.collection.GraphPredef._

object Dots:
  val width = 800
  val height = 800

  val dotSize = 6

  val desiredNum = 36
  val stopNum = 10000
  val maxMisses = 30

  def begin(): GameState = Dots(width, height, Palette.random())

case class Dots(width: Int, height: Int, palette: Palette)
  extends GameState:

  import Dots._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  val walker: algos.PriorityBubbleWalker = algos.PriorityBubbleWalker(
    width = width,
    height = height,
    desiredNum = desiredNum,
    stopNum = stopNum,
    maxMisses = maxMisses,
    None)
    .complete()

  LinkGraph(walker.graph, walker.maxR * 3).build()

  override def next(): Unit = {
  }

  def dot(pixmap: Pixmap)(c: Circle[PriorityValue]): Unit =
    pixmap.setColor(colors(c.v.value))
    pixmap.fillCircle(c.x.toInt, c.y.toInt, dotSize)

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      pixmap.setColor(colors(.1f))
      pixmap.fill()

      pixmap.setColor(colors(1))
      walker.graph.edges.foreach { e =>
        pixmap.drawLine(e.head.x.toInt, e.head.y.toInt,
          e.last.x.toInt, e.last.y.toInt)
      }

      walker.graph.nodes.toOuter
        .foreach(dot(pixmap))
    }
