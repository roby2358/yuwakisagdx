package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{Color, Pixmap}
import org.yuwakisa.flow.Stuff.Conversions.d2f
import org.yuwakisa.flow.algos.ColorIn
import org.yuwakisa.flow.algos.PriorityBubbleWalker.PriorityValue
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.functions._
import org.yuwakisa.flow.model.{Circle, Layer}
import org.yuwakisa.flow.state.Plasma.pascals
import org.yuwakisa.flow.{GameState, Pixmapper, algos, functions}

import scala.util.Random

object ColorMaze:

  val width = 1024
  val height = 1024
  val pix = 1
  
  val palette = Palette.random()

  val dotColors = IndexedSeq[Color](
    Color.RED,
    Color.ORANGE,
    Color.YELLOW,
    Color.GREEN,
    Color.BLUE,
    Color.PURPLE)

  val desiredNum = 36
  val stopNum = 36
  val maxMisses = 100

  def begin(): GameState = ColorMaze(width, height, palette)


case class ColorMaze(width: Int, height: Int, palette: Palette)
  extends GameState:

  import ColorMaze._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  val plasmaLayer: Layer = Layer.zero(width, height) <<
    functions.Plasma.plasmaIsland(8d) <<
    Stretch.histhilo(1000) <<
    Stretch.invertedS <<
//    Swirl.swirl(Random.nextDouble * 2d * math.Pi) <<
    Blur(pascals(16)).gaussian <<
    Identity.apply

  val walker: algos.PriorityBubbleWalker = algos.PriorityBubbleWalker(
    width = width,
    height = height,
    desiredNum = desiredNum,
    stopNum = stopNum,
    maxMisses = maxMisses,
    Some(plasmaLayer))
    .complete()
    
  LinkGraph(walker.graph, walker.maxR * 3).build()

  ColorIn(walker.graph, dotColors).run()

  def dot(pixmap: Pixmap)(c: Circle[PriorityValue]): Unit =
    val r = c.r

    pixmap.setColor(Color.WHITE)
    pixmap.fillCircle(c.x, c.y, walker.minR - 4)
    pixmap.setColor(dotColors(c.color.toInt))
    pixmap.fillCircle(c.x, c.y, walker.minR - 8)

  def render(batch: SpriteBatch): Unit =
    pixmapper.drawLayer(plasmaLayer, pix, colors)

    pixmapper.draw(batch) :
      pixmap =>
        walker.graph.nodes.toOuter
          .foreach({ c =>
            pixmap.setColor(Color.BLACK)
            pixmap.fillCircle(c.x, c.y, walker.minR - 2)
          })

      pixmap.setColor(Color.WHITE)
      walker.graph.edges.foreach : 
        e =>
          pixmap.drawLine(e.head.x, e.head.y,
            e.last.x, e.last.y)

      walker.graph.nodes.toOuter
        .foreach(dot(pixmap))
