package org.yuwakisa.flow.state

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{Color, Pixmap}
import org.json4s._
import org.json4s.native.Serialization
import org.yuwakisa.flow.Stuff.Conversions.d2f
import org.yuwakisa.flow.algos.ColorIn
import org.yuwakisa.flow.algos.PriorityBubbleWalker.PriorityValue
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.functions._
import org.yuwakisa.flow.model.{Circle, Layer}
import org.yuwakisa.flow.state.Galaxy.{height, width}
import org.yuwakisa.flow.state.Plasma.pascals
import org.yuwakisa.flow.stuff.ScreenCap
import org.yuwakisa.flow.{GameState, Pixmapper, algos, functions}

import scala.util.Random

implicit val formats: Formats = DefaultFormats

object TerrainMap:

  val width = 1024
  val height = 1024
  val pix = 1
  val palette = Palette.Terrain66

  val dotColors = IndexedSeq[Color](
    Color(0f, .5f, 1f, 1f),
    Color.GREEN,
    Color(0f, .5f, 0f, 1f),
    Color.ORANGE,

    Color.BLUE,
    Color(.8f, .8f, .2f, 1f),
    Color(0f, .3f, .2f, 1f),
    Color.BROWN)

  def vToDot(v: Double): Int =
    if v < 0.4 then 0
    else if v < 0.7 then 1
    else if v < 0.9 then 2
    else 3

  val desiredNum = 216
  val stopNum = 216
  
  val maxMisses = 100

  def begin(): GameState = TerrainMap(width, height, palette)

case class TerrainMap(width: Int, height: Int, palette: Palette)
  extends GameState:

  import TerrainMap._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)
  var terrainmap: Option[Pixmap] = None

  val plasmaLayer: Layer = Layer.zero(width, height) <<
    functions.Plasma.plasmaIsland(8d) <<
//    Swirl.swirl(math.Pi / 2) <<
//    Blur(8).gaussian <<
    Stretch.histhilo(1000) <<
    Stretch.invertedS <<
    Identity.apply

//    val lushColors = Colors(1000, Palette.random())
//  val lushLayer: Layer = Layer.zero(width, height) <<
//    functions.Plasma.plasmaIsland(2d) <<
//    Stretch.histhilo(1000) <<
//    Identity.apply

  val walker: algos.PriorityBubbleWalker = algos.PriorityBubbleWalker(
    width = width,
    height = height,
    desiredNum = desiredNum,
    stopNum = stopNum,
    maxMisses = maxMisses,
    Some(plasmaLayer))
    .complete()

  LinkGraph(walker.graph, walker.maxR * 3).build()

  // number the nodes
  walker.graph.nodes.map({ n =>
      (n.y * 1000 + n.x, n)
    })
    .toSeq
    .sortWith(_._1 < _._1)
    .zipWithIndex
    .foreach({ a =>
      a._1._2.id = a._2
    })

  walker.graph.nodes.foreach :
    c =>
      c.color = vToDot(plasmaLayer(c.center.x, c.center.y))
  
//    if lushLayer(c.center.x, c.center.y) < 0.5 then
//      c.color += 4

  def dot(pixmap: Pixmap)(c: Circle[PriorityValue]): Unit =
    val r = c.r

    pixmap.setColor(colors(1))
    pixmap.fillCircle(c.x, c.y, walker.minR - 4)
    pixmap.setColor(dotColors(c.color.toInt))
    pixmap.fillCircle(c.x, c.y, walker.minR - 8)

  def render(batch: SpriteBatch): Unit =
    pixmapper.drawOnce(batch) :
      pixmap =>
        pixmapper.drawLayer(plasmaLayer, 1, colors)
        terrainmap = Some(pixmapper.copyPixmap)

        pixmap.setColor(Color.BLACK)
        walker.graph.nodes.toOuter.foreach :
            c =>
              pixmap.fillCircle(c.x, c.y, walker.minR - 2)

        pixmap.setColor(Color.WHITE)
        walker.graph.edges.foreach :
          e =>
            pixmap.drawLine(e.head.x, e.head.y,
              e.last.x, e.last.y)

        walker.graph.nodes.toOuter.foreach(dot(pixmap))

        // TODO:
        // write plasmaLayer
        // write dots
        // write edges

  override def screencap(): Unit =
    terrainmap.foreach:
      px =>
        val sc = ScreenCap()
        sc.write(px)

        case class NodeInfo(id: Int, x: Int, y: Int, color: Int, r: Int)

        val nodes = walker.graph.nodes
          .map(n => NodeInfo(n.id, n.x, n.y, n.color, n.r.toInt))
          .toSeq
          .sortWith(_.id < _.id)

        val edges = walker.graph.edges
          .map(n => Seq(n.head.id, n.last.id))
          .toSeq
          .sortWith({ (a, b) => a(0) * 1000 + a(1) < b(0) * 1000 + b(1) })

        val json = Serialization.write(Map("nodes" -> nodes, "edges" -> edges))

        val file = Gdx.files.local(sc.name + ".json")
        file.writeString(json, false)
