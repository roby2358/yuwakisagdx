package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{Color, Pixmap}
import org.yuwakisa.flow.Stuff.Conversions.d2f
import org.yuwakisa.flow.algos.PriorityBubbleWalker.PriorityValue
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.functions._
import org.yuwakisa.flow.model.{Circle, Layer}
import org.yuwakisa.flow.state.Plasma.pascals
import org.yuwakisa.flow.{GameState, Pixmapper, algos, functions}

import scala.util.Random

object PlasmaDots:

  val width = 800
  val height = 800
  val pix = 1

  val desiredNum = 36
  val stopNum = 36
  val maxMisses = 100

  def begin(): GameState = PlasmaDots(width, height, Palette.random())

case class PlasmaDots(width: Int, height: Int, palette: Palette) extends GameState:

  import PlasmaDots._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  val plasmaLayer: Layer = Layer.zero(width, height) <<
    functions.Plasma.plasma(4d) <<
    Stretch.histhilo(1000) <<
    Swirl.swirl(Random.nextDouble * 2d * math.Pi) <<
    Blur(pascals(16)).gaussian <<
    Identity.apply

  val walker: algos.PriorityBubbleWalker = algos.PriorityBubbleWalker(
    width = width,
    height = height,
    desiredNum = desiredNum,
    stopNum = stopNum,
    maxMisses = maxMisses,
    Some(plasmaLayer))
    .complete()

  LinkGraph(walker.graph, walker.maxR * 3).build()

  override def next(): Unit = ()

  def dot(pixmap: Pixmap)(c: Circle[PriorityValue]): Unit =
    //    val r = sqr(1 - plasmaLayer(c.x, c.y)) *
    //      (c.r - walker.minR) +
    //      walker.minR

    val r = c.r

    pixmap.setColor(colors(1))
//    pixmap.fillCircle(c.x, c.y, r - 6)
    pixmap.fillCircle(c.x, c.y, walker.minR)
    pixmap.setColor(colors(plasmaLayer(c.x, c.y)))
//    pixmap.fillCircle(c.x, c.y, r - 12)
    pixmap.fillCircle(c.x, c.y, walker.minR - 6)

  def render(batch: SpriteBatch): Unit =
    pixmapper.drawLayer(plasmaLayer, pix, colors)

    pixmapper.draw(batch) { pixmap =>
      walker.graph.nodes.toOuter
        .foreach({ c =>
          pixmap.setColor(Color.BLACK)
          //    pixmap.fillCircle(c.x, c.y, r - 12)
          pixmap.fillCircle(c.x, c.y, walker.minR + 2)
        })

      pixmap.setColor(colors(1))
      walker.graph.edges.foreach { e =>
        pixmap.drawLine(e.head.x, e.head.y,
          e.last.x, e.last.y)
      }

      walker.graph.nodes.toOuter
        .foreach(dot(pixmap))
    }
