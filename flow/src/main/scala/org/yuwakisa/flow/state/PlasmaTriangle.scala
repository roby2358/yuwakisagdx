package org.yuwakisa.flow.state

import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.functions.{Identity, Stretch}
import org.yuwakisa.flow.model.{Layer, PascalsTriangle, StaticGrid}
import org.yuwakisa.flow.{GameState, functions}

object PlasmaTriangle:

  val width = 400
  val height = 400
  val pix = 2

  val pascals: Seq[Seq[Int]] = PascalsTriangle(35).triangle

  def begin(): GameState =
    StaticGrid(
      Layer.zero(width, height) <<
        functions.PlasmaTriangle.plasma <<
        Stretch.hilo <<
        //        Blur(pascals(16)).apply <<
        //        Contor.apply <<
        Identity.apply,
      pix,
      Palette.random())
