package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{FPSLogger, Pixmap}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model._
import org.yuwakisa.flow.stuff.RandomQueue
import org.yuwakisa.flow.{GameState, Pixmapper}
import scalax.collection.GraphEdge.DiEdge
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph

import scala.collection.mutable
import scala.util.Random

object BubbleWalk:

  val width = 800
  val height = 800

  val maxMisses = 20

  def begin(): GameState = BubbleWalk(width, height, Palette.random())

case class BubbleWalk(width: Int, height: Int, palette: Palette) extends GameState :

     import BubbleWalk._

     lazy val pixmapper: Pixmapper = Pixmapper(width, height)

     val boundary: Rectangle = Rectangle(0, 0, width, height)

     val desiredNum = 100
     val maxR: Double = math.max(math.sqrt((width * height) /
       (desiredNum * math.Pi)), 10)
     val minR: Double = math.max(maxR / 4, 5)

     val graph: Graph[Circle[Double], DiEdge] = Graph()
     val queue: RandomQueue[Circle[Double]] = new RandomQueue[Circle[Double]]()
     val misses: mutable.Map[Circle[Double], Int] = mutable.Map[Circle[Double], Int]()
       .withDefaultValue(0)

     val r1: Double = PackedCircles.rr
     val x1: Double = Random.nextInt(width - 2 * r1.toInt) + r1
     val y1: Double = Random.nextInt(height - 2 * r1.toInt) + r1
     val first: Circle[Double] = Circle(x1, y1, r1, Random.nextDouble())

     graph += first
     queue += first

     def isValid(c: Circle[Double]): Boolean = c.within(boundary) &&
       graph.nodes.forall(!_.collide(c))

     def visit(c: Circle[Double]): Unit =
       val (vx, vy) = Cell.valueToVector(Random.nextDouble())
       // (Random.nextGaussian() / 12) *
       val r = (1 + Random
         .nextGaussian() / 3) * ((maxMisses.toDouble - misses(c)) / maxMisses) *
         (maxR - minR) + minR
       val d = Circle(c.x + vx * (r + c.r),
         c.y + vy * (r + c.r),
         r,
         Cell.minMax(c.v.value + Random.nextGaussian() / 12d))

       if isValid(d) then
         graph += d
         graph += (d ~> c)
         queue += d
         queue += c
       else
         misses.update(c, misses(c) + 1)
         if misses(c) < maxMisses then
           queue += c
         if queue.nonEmpty then visit(queue.dequeue())

     def findClosestAndFarthest(p: Point): (Circle[Double], Circle[Double]) =
       (graph.nodes.minBy(c => p.distance(c.center)),
         graph.nodes.maxBy(c => p.distance(c.center)))

     lazy val finish: Unit =
       println("done! " + graph.nodes.size)

     override def next(): Unit =
       if graph.nodes.size >= desiredNum then
         queue.clear()
         finish
       else if queue.nonEmpty then
         visit(queue.dequeue())
       else
         finish

     def circle(pixmap: Pixmap)(c: Circle[Double]): Unit =
       pixmap.setColor(colors(c.v.value.toFloat))
       pixmap.drawCircle(c.x.toInt, c.y.toInt, c.r.toInt)

     val fpsLogger = new FPSLogger()

     def render(batch: SpriteBatch): Unit =
       pixmapper.draw(batch) { pixmap =>
         fpsLogger.log()

         pixmap.setColor(colors(.4f))
         pixmap.fill()

         graph.nodes.toOuter
           .foreach(circle(pixmap))

         pixmap.setColor(colors(0.2f))
         graph.edges.foreach { e =>
           pixmap.drawLine(e.head.x.toInt, e.head.y.toInt,
             e.last.x.toInt, e.last.y.toInt)
         }
       }
