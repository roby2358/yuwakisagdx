package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Disposable
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model.Point
import org.yuwakisa.flow.{GameState, Pixmapper}

import scala.util.Random

/**
 * I think this was supposed to plot random points and then draw a mesh between
 * them
 */
object PackedTriangles:
  val pix = 4
  val count = 20

  def begin(): GameState = PackedTriangles(400, 400, Palette.random())

case class PackedTriangles(height: Int, width: Int, palette: Palette)
  extends GameState with Disposable:

  import PackedTriangles._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  def rr: Int =
    val r = Random.nextInt(width - 20) + 10
    r / 10 * 10

  var currentColor: Float = Random.nextFloat()

  var last: Long = 0

  var points: Seq[Point] = Seq()

  def now: Long = System.currentTimeMillis

  override def next(): Unit =
    if now > last + 5000 then
      last = now

//      colors = Colors(Colors.random())
      currentColor = Random.nextFloat()

      points = Seq.fill(count)({
        Point(rr, rr)
      })

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      pixmap.setColor(colors(.2f))
      pixmap.fill()

      pixmap.setColor(colors(currentColor))
      points.foreach({ p =>
        if pix == 1 then pixmap.drawPixel(p.x.toInt, p.y.toInt)
        else pixmap.fillRectangle(p.x.toInt, p.y.toInt, pix, pix)
      })
    }

  def dispose(): Unit = pixmapper.dispose()
