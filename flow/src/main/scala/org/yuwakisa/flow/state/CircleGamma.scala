package org.yuwakisa.flow.state

import breeze.linalg.*
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.utils.Disposable
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model.PackedCircles
import org.yuwakisa.flow.{GameState, Pixmapper}

object CircleGamma:

  val width = 800
  val height = 800

  def begin(): GameState = CircleGamma(width, height, Palette.random())

case class CircleGamma(width: Int, height: Int, palette: Palette)
  extends GameState with Disposable {

  val offX: Int = width / 2
  val offY: Int = height / 2
  val pixmapper: Pixmapper = Pixmapper(width, height)

  var circles: PackedCircles = PackedCircles(PackedCircles.rr)
    .makeCircles()

  def now: Long = System.currentTimeMillis

  var last: Long = 0

  override def next(): Unit =
    if now > last + 1000 then
      last = now

      circles = PackedCircles(PackedCircles.rr).makeCircles()

  var angle = 0d

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      pixmap.setColor(Color.BLACK)
      pixmap.fill()

      pixmap.setColor(Color.WHITE)
      pixmap.drawCircle(offX, offY, circles.r1.toInt)

      val r = circles.rotate(angle) * circles.centers
      angle += math.Pi / 600

      var i = 0
      r(::, *).foreach({ r =>
        val rr = r.toArray
        pixmap.drawCircle(offX + rr(0).toInt,
          offY - rr(1).toInt,
          circles.radii(i).toInt)
        i += 1
      })
    }

  def dispose(): Unit = pixmapper.dispose()
}