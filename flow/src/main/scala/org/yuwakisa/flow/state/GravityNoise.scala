package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import org.yuwakisa.flow.algos.Halton
import org.yuwakisa.flow.colorwheel.{Colors, Luminance, Palette}
import org.yuwakisa.flow.functions.{Identity, Stretch}
import org.yuwakisa.flow.model.{Layer, Point, StaticGrid}
import org.yuwakisa.flow.{GameState, Pixmapper, functions}

import scala.util.Random

object GravityNoise:

  val width = 400
  val height = 400
  val pix = 1

  def sqr(x: Double): Double = x * x

  object Gravity:

    def jagged(d: Double): Double = 1.0 / sqr(1 + d) * (Random.nextDouble())

    def fallingcos(d: Double): Double = ((math.cos(d * 10 * math.Pi) + 1) / 2) * math.exp(-d)

    def gravitywaves(d: Double): Double = ((math.cos(d * 10 * math.Pi) + 1) / 2) / sqr(1 + d)

    def invsquaredist(d: Double): Double = 1.0 / sqr(1 + d)

    val apply: Double => Double = gravitywaves

  class Bell(a: Double, center: Point, c: Double) :
    def value(p: Point): Double =
      val d = p.distance(center)
      if d > c * 10 then
        return 0
      a * Gravity.apply(d / c)

  object Bells:
    def plusMinusOne = Random.nextDouble() * 2 - 1
    
    def gaussian = Random.nextGaussian() / 3

    def zeroOne = Random.nextDouble()

    def one = 1d

    def randomAt(p: Point, scale: Double): Bell =
      val a = math.exp(scale / width) * plusMinusOne
//      val a = scale / width * plusMinusOne
      val c = scale * math.sqrt(3) / 3
      Bell(a, p, c)

    def random(scale: Double): Bell =
      val center = Point(width * Random.nextDouble(),
        height * Random.nextDouble())
      randomAt(center, scale)

    def randoms(count: Int)(scale: Int) =
      Seq.fill(count) { random(scale) }

    val hasqrt3: Double = math.sqrt(3) / 2

    def onTriangs(scale: Int) =
      val yscale = (scale * hasqrt3).toInt
      val padx = (width - scale * (width.toDouble / scale - 1).toInt - scale / 2) / 2
      val pady = (height - yscale * (height.toDouble / yscale - 1).toInt) / 2
      for (xi <- 0 until width / scale ;
           yi <- 0 until height / yscale)
        yield
          val off = if yi % 2 == 0 then scale / 2 else 0
          val x = xi * scale + off
          val y = yi * yscale 
          randomAt(Point(x + padx, y + pady), scale)

    def halton(n: Int, scale: Int) =
      val hx = Halton.random()
      val hy = Halton.random()
      (0 until n).map :
        _ =>
          val x = (hx.next() * width).toInt
          val y = (hy.next() * height).toInt
          randomAt(Point(x, y), scale)

  val primes = Seq(3, 5, 7, 11, 13, 17, 19)

  val fibbo = Seq(3, 5, 8, 13, 21, 34, 55)

  val twos = Seq(4, 8, 16, 32, 64)

  def begin(): GameState =
    val bells = fibbo.slice(1, 4).flatMap { i => Bells.halton(i * i * 2, width / i) }
//    val bells = fibbo.slice(3,5).map(width/_).flatMap(Bells.onTriangs)

    println(s"Bells: ${bells.length}")

    var layer = Layer.zero(width, height)

    layer.foreach :
      (x, y) =>
          val p = Point(x, y)
          var v = 0d
          bells.foreach { b => v = v + b.value(p) }
          layer.set(x, y, v.toFloat)

    layer = layer <<
      Stretch.histhilo(1000) <<
//      Stretch.invertedS <<
      Identity.apply

//    val colors = Luminance.linear(0.1, 0.5)(Palette.random())
    val colors = Luminance.linear(0.1, 0.5)(Palette.random())

    StaticGrid(layer, pix, colors)
