package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.{GameState, Pixmapper}

import scala.annotation.tailrec
import scala.util.Random

object Concentric:

  val width = 800
  val height = 800

  def begin(): GameState = Concentric(width, height, Palette.random())

case class Concentric(width: Int, height: Int, palette: Palette) extends GameState:

  import Concentric._

  val xx = width / 2
  val yy = height / 2

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  def f(a: Double): Double =
    (1 + Random.nextGaussian() / 12) *
      math.sin(7 * a * math.Pi * 2) *
      .3 +
      .8

  def circle(pixmap: Pixmap)(r: Int): Unit =
    val cc = 2 * math.Pi * r * .707
    var px = -1
    var py = -1
    (0 until cc.toInt).foreach({ arc =>
      val a = arc / cc * 2 * math.Pi
      val rr = r * f(arc / cc)
      val x = math.cos(a) * rr
      val y = math.sin(a) * rr

      if px < 0 || py < 0 then
        pixmap.drawPixel((xx + x).toInt, (yy + y).toInt)
      else
        pixmap.drawLine(px, py, (xx + x).toInt, (yy + y).toInt)

      px = (xx + x).toInt
      py = (yy + y).toInt
    })

  @tailrec private def whiledo[A, B](a: A)
                            (first: A => A)
                            (test: A => Boolean)
                            (last: A => A): A =
      val b = first(a)
      if test(b) then
        b
      else
        whiledo(last(b))(first)(test)(last)

  def render(batch: SpriteBatch): Unit =
    pixmapper.drawOnce(batch) { pixmap =>

      pixmap.setColor(colors(.4f))
      pixmap.fill()

      pixmap.setColor(colors(1f))
      (100 to 300 by 100).foreach(circle(pixmap))
    }
