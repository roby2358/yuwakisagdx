package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import org.yuwakisa.flow.GameState
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}

object Plain:

  val width = 800
  val height = 800

  def begin(): GameState = Plain(width, height, Palette.random())

case class Plain(width: Int, height: Int, palette: Palette)
  extends GameState:

  def render(batch: SpriteBatch): Unit = {
  }
