package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.{Pixmap, Texture}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.functions.Identity
import org.yuwakisa.flow.model.{Layer, PascalsTriangle}
import org.yuwakisa.flow.{GameState, functions}

object Krackle:

  val width = 800
  val height = 800
  val pix = 1

  val pascals: Seq[Seq[Int]] = PascalsTriangle(35).triangle

  def begin(): GameState =
    new Krackle(
      pix,
      Palette.random(),
      Layer.zero(width, height) <<
        //        Plasma.apply <<
        functions.Krackle.apply <<
        //        Blur(pascals(4)).apply <<
        Identity.apply)

class Krackle(pix: Int,
              val palette: Palette,
              val grid: Layer) extends GameState:

  val height: Int = grid.height * pix

  val width: Int = grid.width * pix

  lazy val pixmap = new Pixmap(width, height, Format.RGB888)

  val plot: (Int, Int) => Unit =
    if pix == 1 then pixmap.drawPixel
    else pixmap.fillRectangle(_, _, pix, pix)

  def renderPixel(shapeRenderer: ShapeRenderer, x: Int, y: Int): Unit =
    shapeRenderer.rect(x.toFloat, y.toFloat, 1, 1)

  def renderRectantgle(shapeRenderer: ShapeRenderer, x: Int, y: Int): Unit =
    shapeRenderer.rect(x.toFloat * pix, y.toFloat * pix, pix.toFloat,
      pix.toFloat)

  override def render(batch: SpriteBatch): Unit =
    pixmap.setColor(colors(1))
    pixmap.fill()

    grid.foreachPrimey({ (x, y) =>
      val v = grid(x, y)
      if v >= 0.1f then {
//        pixmap.setColor(Color.BLACK)
        pixmap.setColor(colors(1 - v))
        pixmap.fillCircle(x * pix, y * pix, (10 * v).toInt)
      }
    })

    val text = new Texture(pixmap)
    batch.begin()
    batch.draw(text, 0, 0)
    batch.end()
    text.dispose()

  def dispose(): Unit =
    pixmap.dispose()
