package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{FPSLogger, Pixmap}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model._
import org.yuwakisa.flow.stuff.RandomQueue
import org.yuwakisa.flow.{GameState, Pixmapper}
import scalax.collection.GraphEdge.DiEdge
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph

import scala.collection.mutable
import scala.util.Random

object ChargedBubbleWalk:

  val width = 1000
  val height = 1000

  val desiredNum = 5000
  val maxMisses = 20

  def begin(): GameState = ChargedBubbleWalk(width, height, Palette.random())

case class ChargedBubbleWalk(width: Int, height: Int, palette: Palette) extends GameState:

  import ChargedBubbleWalk._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  val boundary: Rectangle = Rectangle(0, 0, width, height)

  val maxR: Double = math.max(math.sqrt((width * height * 0.9) /
    (desiredNum * math.Pi)), 15)
  val minR: Double = math.max(maxR / 4, 3)

  val graph: Graph[Circle[Double], DiEdge] = Graph()
  val queue: RandomQueue[Circle[Double]] = new RandomQueue[Circle[Double]]()
  val misses: mutable.Map[Circle[Double], Int] = mutable.Map[Circle[Double], Int]()
    .withDefaultValue(0)

  val r1: Double = PackedCircles.rr
  val x1: Double = Random.nextInt(width - 2 * r1.toInt) + r1
  val y1: Double = Random.nextInt(height - 2 * r1.toInt) + r1
//  val first: Circle = Circle(x1, y1, r1, Random.nextDouble())
  val first: Circle[Double] = Circle(width / 2, height / 2, r1, (1 + Random
    .nextDouble() / 9) * (maxR / minR) / 2)

  graph += first
  queue += first

  def isValid(c: Circle[Double]): Boolean = c.within(boundary) &&
    graph.nodes.forall(!_.collide(c))

  def visit(c: Circle[Double]): Unit =
    val (vx, vy) = Cell.valueToVector(Random.nextDouble())
    // (Random.nextGaussian() / 12) *
    val r = ((maxMisses.toDouble - misses(c)) / maxMisses) *
      (maxR - minR) + minR
    val d = Circle[Double](c.x + vx * (r + c.r), c.y + vy * (r + c.r), r, 0)

    if isValid(d) then
      graph += d
      graph += (d ~> c)
      queue += d
      queue += c

      graph.get(d).outerNodeTraverser.foreach({n =>
        n.take(n.v + 20f / desiredNum)
      })
    else
      misses.update(c, misses(c) + 1)
      if misses(c) < maxMisses then
        queue += c
        if queue.nonEmpty then visit(queue.dequeue())

  def findClosestAndFarthest(p: Point): (Circle[Double], Circle[Double]) =
    (graph.nodes.minBy(c => p.distance(c.center)),
      graph.nodes.maxBy(c => p.distance(c.center)))

  lazy val finish: Unit =
    println("done! " + graph.nodes.size)

  override def next(): Unit =
    if graph.nodes.size >= desiredNum then
      queue.clear()
      finish
    else if queue.nonEmpty then
      visit(queue.dequeue())
    else
      finish

  def circle(pixmap: Pixmap)(c: Circle[Double]): Unit =
    pixmap.setColor(colors(c.v.value.toFloat))
    pixmap.drawCircle(c.x.toInt, c.y.toInt, c.r.toInt)

  val fpsLogger = new FPSLogger()

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      fpsLogger.log()

      pixmap.setColor(colors(20f / desiredNum))
      pixmap.fill()

//      graph.nodes
//        .map(_.value)
//        .foreach(circle(pixmap))

      graph.edges.toOuter.foreach { e =>
        pixmap.setColor(colors(e.head.v.value.toFloat))
        pixmap.drawLine(e.head.x.toInt, e.head.y.toInt,
          e.last.x.toInt, e.last.y.toInt)
      }
    }
