package org.yuwakisa.flow.state

import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{FPSLogger, Pixmap}
import org.yuwakisa.flow.colorwheel.Palette.Palette
import org.yuwakisa.flow.colorwheel.{Colors, Palette}
import org.yuwakisa.flow.model._
import org.yuwakisa.flow.stuff.RandomQueue
import org.yuwakisa.flow.{GameState, Pixmapper}
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph

import scala.collection.mutable
import scala.util.Random

object BubbleMaze:
  val maxMisses = 10
  val desiredNum = 1000

  def begin(): GameState = BubbleMaze(800, 800, Palette.Grayscale)

case class BubbleMaze(width: Int, height: Int, palette: Palette) extends 
  GameState:

  import BubbleMaze._

  lazy val pixmapper: Pixmapper = Pixmapper(width, height)

  val boundary: Rectangle = Rectangle(0, 0, width, height)

  val maxR: Double = math.max(math.sqrt((width * height) /
    (.9f * desiredNum * math.Pi)), 10)
  val minR: Double = math.max(maxR / 4, 5)

  println(s"$minR $maxR")

  val graph: Graph[Circle[Double], UnDiEdge] = Graph()
  val queue: RandomQueue[Circle[Double]] = new RandomQueue[Circle[Double]]()
  val misses: mutable.Map[Circle[Double], Int] = mutable.Map[Circle[Double], Int]()
    .withDefaultValue(0)

  val r1: Double = PackedCircles.rr
  val x1: Double = Random.nextInt(width - 2 * r1.toInt) + r1
  val y1: Double = Random.nextInt(height - 2 * r1.toInt) + r1
  val first: Circle[Double] = Circle(x1, y1, r1, Random.nextDouble())

  graph += first
  queue += first

  def isValid(c: Circle[Double]): Boolean = c.within(boundary) &&
    graph.nodes.forall(!_.collide(c))

  def visit(c: Circle[Double]): Option[Circle[Double]] =
    val (vx, vy): (Double, Double) = Cell.valueToVector(Random.nextDouble())
    // (Random.nextGaussian() / 12) *
    val r = ((maxMisses.toDouble - misses(c)) / maxMisses) *
      (maxR - minR) + minR
    val d = Circle(c.x + vx * (r + c.r),
      c.y + vy * (r + c.r),
      r,
      Cell.minMax(c.v.value + Random.nextGaussian() / 12d))

    if isValid(d) then
      graph += d
      graph += (c ~ d)
      queue += d
      queue += c
      Some(c)
    else
      misses.update(c, misses(c) + 1)
      if misses(c) < maxMisses then
        queue += c
      visit(queue.dequeue())

  def findClosestAndFarthest(p: Point): (Circle[Double], Circle[Double]) =
    (graph.nodes.minBy(c => p.distance(c.center)),
      graph.nodes.maxBy(c => p.distance(c.center)))

  lazy val finish: Unit =
    println("done! " + graph.nodes.size)

  override def next(): Unit =
    if graph.nodes.size >= desiredNum then
      queue.clear()
      finish
    else if queue.nonEmpty then
      visit(queue.dequeue())
    else
      finish

  def circle(pixmap: Pixmap)(c: Circle[Double]): Unit =
    pixmap.setColor(colors(c.v.value))
    pixmap.drawCircle(c.x.toInt, c.y.toInt, c.r.toInt)

  val fpsLogger = new FPSLogger()

  def render(batch: SpriteBatch): Unit =
    pixmapper.draw(batch) { pixmap =>
      fpsLogger.log()

      pixmap.setColor(colors(.3f))
      pixmap.fill()

//      graph.nodes
//        .map(_.value)
//        .foreach(circle(pixmap))

      val (near, far) = findClosestAndFarthest(Point(0, 0))
      val farD = far.distance(Point(0,0))

      graph.edges.toOuter.foreach { e =>
        val a = e.head
        val b = e.last
        val d = a.distance(Point(0,0))
        val cc = 1 - (d / farD) * .4
        pixmap.setColor(colors(cc))
        pixmap.drawLine(a.x.toInt, a.y.toInt, b.x.toInt, b.y.toInt)
      }

      pixmap.setColor(colors(0f))
      pixmap.fillCircle(near.x.toInt, near.y.toInt, near.r.toInt)
      pixmap.fillCircle(near.x.toInt, near.y.toInt, near.r.toInt)
      pixmap.fillCircle(far.x.toInt, far.y.toInt, far.r.toInt)
      pixmap.setColor(colors(1f))
      pixmap.fillCircle(far.x.toInt, far.y.toInt, far.r.toInt / 2)
    }
