package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Circle, LineSegment, Point}
import scalax.collection.GraphEdge.UnDiEdge
import scalax.collection.GraphPredef._
import scalax.collection.mutable.Graph

case class LinkGraph[A](graph: Graph[Circle[A], UnDiEdge], maxR: Double):

  def notTooLong(l: LineSegment): Boolean = l.length < maxR

  def noCollision(l: LineSegment)(e: graph.EdgeT): Boolean =
    if l.a == e.head.center || l.a == e.last.center ||
      l.b == e.head.center || l.b == e.last.center then
      return true
    l.intersect(LineSegment(e.head.center, e.last.center)).isEmpty

  def notTooClose(l: LineSegment)(c: Circle[_]): Boolean =
    if l.a == c.center || l.b == c.center then
      return true
    l.distance(c.center) > c.r
    
  def build(): Unit =
    val dots: IndexedSeq[Circle[A]] =
      graph.nodes.toOuter.toIndexedSeq

    val pairs = for i <- dots.indices;
                     j <- i + 1 until dots.length
      yield
        (dots(i), dots(j))

    pairs
      .sortBy({ case (c, d) => c.distance(d) })
      .foreach({ case (c, d) =>
        val e = c ~ d
        val l = LineSegment(c.center, d.center)
        if !graph.edges.contains(e) &&
          notTooLong(l) &&
          graph.edges.forall(noCollision(l)) &&
          dots.forall(notTooClose(l)) then {
          graph += e
        }
      })
