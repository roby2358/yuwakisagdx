package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Grid, Layer}

object Contor:
  val totes = 100
  val keep = 10

  def blackSpace(v: Float) =
    val vv: Int = math.floor(totes * v).toInt
    if vv % keep == 0 then vv.toFloat / totes else 0f

  def cycle(v: Float) =
    val vv: Int = math.floor(totes * v).toInt
    (vv % keep).toFloat / keep

  def vcycle(v: Float) =
    val v0 = math.floor(totes * v).toInt
    val v1 = (v0 % (keep * 2)).toFloat / keep
    if v1 <= 1f then v1 else 2f - v1

  def apply(g: Layer): Layer = g.newValues({ (x, y) => vcycle(g(x, y)) })

  def apply(g: Grid): Grid = g.copy(values = this (g.values))
