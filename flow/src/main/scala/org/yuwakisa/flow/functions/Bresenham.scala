package org.yuwakisa.flow.functions

import org.yuwakisa.flow.Stuff.Doer
import org.yuwakisa.flow.model.Point

object Bresenham:

  def ray(a: Point, rads: Double, d: Double): Bresenham =
    Bresenham(a, Point(a.x + math.cos(rads) * d, a.y + math.sin(rads) * d))

case class Bresenham(a: Point, b: Point):

  def line: Seq[Point] =

    val sx = math.signum(b.x - a.x).toInt
    val sy = math.signum(b.y - a.y).toInt
    val dx = math.abs(b.x - a.x).toInt
    val dy = math.abs(b.y - a.y).toInt
    var ddx = 2 * dy - dx
    var ddy = 2 * dx - dy
    var x: Int = 0
    var y: Int = 0

    (0 to math.max(dx, dy)).map({ _ =>
      Point(a.x.toInt + x, a.y.toInt + y).tap({ _ =>
        if ddx > 0 then {
          y += sy
          ddx -= 2 * dx
        }
        if ddy > 0 then {
          x += sx
          ddy -= 2 * dy
        }
        ddx += 2 * dy
        ddy += 2 * dx
      })
    })
