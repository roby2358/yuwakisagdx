package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Grid, Layer, PascalsTriangle}

object Blur:

  val pascals: Seq[Seq[Int]] = PascalsTriangle(35).triangle

  def apply(n: Int): Blur = apply(pascals(n))

case class Blur(weight: Seq[Int]):

  def idx(x: Int, xx: Int, length: Int): Int = xx - x + length / 2

  def range(weight: Seq[Int], i: Int, lastX: Int): Range =
    val haLength = weight.length / 2
    math.max(0, i - haLength) until math.min(lastX, i + haLength)

  def weightSum(weight: Seq[Int], s: Range, i: Int): Int =
    s.map({ ii => weight(idx(i, ii, weight.length)) }).sum

  def blurX(g: Layer, x: Int, y: Int, weight: Seq[Int]): Float =
    val s = range(weight, x, g.lastX)
    val ss = weightSum(weight, s, x)
    s.map({ xx =>
      g(xx, y) * weight(idx(x, xx, weight.length)) / ss
    }).sum

  def blurY(g: Layer, x: Int, y: Int, weight: Seq[Int]): Float =
    val s = range(weight, y, g.lastY)
    val ss = weightSum(weight, s, y)
    s.map({ yy =>
      g(x, yy) * weight(idx(y, yy, weight.length)) / ss
    }).sum

  def gaussian(g: Layer): Layer =
    val blurredX = g.newValues({ case (x, y) =>
      blurX(g, x, y, weight)
    })

    blurredX.newValues({ case (x, y) =>
      blurY(blurredX, x, y, weight)
    })

  def grid(g: Grid): Grid = g.copy(values = gaussian(g.values))
