package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Grid, Layer}

import scala.util.Random

case class Splatter(n: Int):

  def apply(g: Layer): Layer =
    (0 to n).foreach({ _ =>
      val x = Random.nextInt(g.width)
      val y = Random.nextInt(g.height)
      g.cells(y)(x) = 1
    })
    g

  def apply(g: Grid): Grid = g.copy(values = this (g.values))
