package org.yuwakisa.flow.functions

import org.yuwakisa.flow.Stuff.Conversions.d2i
import org.yuwakisa.flow.model.{Grid, Layer}

import scala.collection.mutable

object Stretch:

  def hilo(g: Layer): Layer =
    val (min, max) = g.minMax
    g.newValues({ (x, y) =>
      (g(x, y) - min) / (max - min)
    })

  def s1hilo(g: Layer): Layer =
    val (min, max) = g.minMax
    g.newValues({ (x, y) =>
      val v = (g(x, y) - min) / (max - min)
      v * v * (3 - 2 * v)
    })

  def histhilo(l: Int)(g: Layer): Layer =
    val (min, max) = g.minMax

    def v(x: Int, y: Int): Double = (l - 1) * (g(x, y) - min) / (max - min)

    val h = mutable.ArrayBuffer.fill(l)(0)
    g.foreach({ (x, y) => h(v(x, y)) += 1 })

    val cdf = mutable.ArrayBuffer.fill(l)(0f)
    cdf.indices.foreach({ i =>
      if i > 0 then
        cdf(i) = cdf(i - 1) + h(i)
    })

    val total = cdf(l - 1)
    g.newValues({ (x, y) => cdf(v(x, y)) / total })

  def invs(x: Double) = 2.85714 * x - 5.71429 * x * x + 3.80952 * x * x * x

  def invertedS(g: Layer): Layer =
    val (min, max) = g.minMax
    g.newValues({ (x, y) =>
      invs((g(x, y) - min) / (max - min)).toFloat
    })

  def grid(g: Grid): Grid = g.copy(values = hilo(g.values))
