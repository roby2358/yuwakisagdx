package org.yuwakisa.flow.functions

case class WeightedAverage(weights: Seq[Double]):

  lazy val denum: Double = weights.sum

  /**
   * ordinary old-fashioned weighted average
   */
  def linear(v: Seq[Double]): Double = v.sum / denum
