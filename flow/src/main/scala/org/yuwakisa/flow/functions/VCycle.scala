package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Grid, Layer}

object VCycle:

    def apply(g: Layer): Layer =
        g.newValues((x, y) => {
            val v = g(x, y) * 2
            if v <= 1 then v else 2 - v
        })

    def apply(g: Grid): Grid = g.copy(values = this(g.values))
