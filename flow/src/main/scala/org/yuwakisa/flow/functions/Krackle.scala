package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Cell, Grid, Layer}

import scala.util.Random

object Krackle:

  def jump(x: Int, y: Int, jumpX: Double, jumpY: Double): (Int, Int) =
    val xx = x + ((Random.nextDouble() - 0.5) * jumpX * 2).toInt
    val yy = y + ((Random.nextDouble() - 0.5) * jumpY * 2).toInt
    (xx, yy)

  def gaussianJump(x: Int, y: Int, jumpX: Double, jumpY: Double): (Int, Int)
  =
    val xx = x + (Random.nextGaussian() / 3d * jumpX * 2).toInt
    val yy = y + (Random.nextGaussian() / 3d * jumpY * 2).toInt
    (xx, yy)

  def angularJump(x: Int, y: Int, jumpX: Double, jumpY: Double): (Int, Int) =
    val (vx, vy) = Cell.valueToVector(Random.nextDouble())
    val d = Random.nextDouble()
    val xx = x + (vx * jumpX * d).toInt
    val yy = y + (vy * jumpY * d).toInt
    (xx, yy)

  def apply(g: Layer): Layer =
    val jumpX = 20
    val jumpY = 20
    var x = Random.nextInt(g.width)
    var y = Random.nextInt(g.height)
    val count: Int = g.width * g.height / 200

    (0 until count).indices.foreach({ _ =>
      g.cells(y)(x) = Random.nextDouble().toFloat

      val (xx, yy) = jump(x, y, jumpX, jumpY)
      x = xx
      y = yy

      //      x = x + 3 * (if (x > g.width / 2) 1 else -1)
      //      y = y + 3 * (if (y > g.width / 2) 1 else -1)

      val dx = g.width / 2 - x
      val dy = g.height / 2 - y
      val d = math.sqrt(dx * dx + dy * dy)

      if d > 0 then {
        x = (x - 3 * dx / d).toInt
        y = (y - 3 * dy / d).toInt
      }

      if x < 0 || x >= g.width || y < 0 || y >= g.height then {
        x = Random.nextInt(g.width)
        y = Random.nextInt(g.height)
      }
    })
    g

  def apply(g: Grid): Grid = g.copy(values = this (g.values))
