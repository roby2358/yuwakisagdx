package org.yuwakisa.flow.functions

import breeze.linalg.DenseMatrix
import org.yuwakisa.flow.model.{Layer, Point}

object Swirl:

  def swirl(a: Double)(layer: Layer): Layer =
    val newLayer = Layer.zero(layer.width, layer.height)

    val translation0: DenseMatrix[Double] =
      DenseMatrix((1d, 0d, -layer.width.toDouble / 2d),
        (0d, 1d, -layer.height.toDouble / 2d),
        (0d, 0d, 1d))
    val translation1: DenseMatrix[Double] =
      DenseMatrix((1d, 0d, layer.width.toDouble / 2d),
        (0d, 1d, layer.height.toDouble / 2d),
        (0d, 0d, 1d))

    def rotate(theta: Double): DenseMatrix[Double] =
      DenseMatrix((math.cos(theta), math.sin(theta), 0d),
        (-math.sin(theta), math.cos(theta), 0d),
        (0d, 0d, 1d))

    val center = Point(layer.width / 2, layer.height / 2)

    layer.foreach({ (x, y) =>
      val p0 = Point(x, y)
      val g = 2d * p0.distance(center) / layer.width
      val p = translation1 * rotate(g * g * a) * translation0 * p0.matrix
      val pp = Point(p)

      val v = layer(x, y)
      val xx = pp.x.toInt
      val yy = pp.y.toInt
      Seq(
        (-2, -2, 1),
        (-1, -2, 4),
        (0, -2, 7),
        (1, -2, 4),
        (2, -2, 1),

        (-2, -1, 4),
        (-1, -1, 16),
        (0, -1, 26),
        (1, -1, 16),
        (2, -1, 4),

        (-2, 0, 7),
        (-1, 0, 26),
        (0, 0, 41),
        (1, 0, 26),
        (2, 0, 7),

        (-2, 1, 4),
        (-1, 1, 16),
        (0, 1, 26),
        (1, 1, 16),
        (2, 1, 4),

        (-2, 2, 1),
        (-1, 2, 4),
        (0, 2, 7),
        (1, 2, 4),
        (2, 2, 1))
        .map({ case (dx, dy, dv) =>
          (xx + dx, yy + dy, v * dv / 273d)
        })
        .filter({ case (x, y, v) =>
          x >= 0 && x < layer.width && y >= 0 && y < layer.height
        })
        .foreach({ case (x, y, v) =>
          newLayer.add(x, y, v.toFloat)
        })
    })
    newLayer
