package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Grid, Layer}

object Identity:

  def apply(g: Layer): Layer = g

  def apply(g: Grid): Grid = g
