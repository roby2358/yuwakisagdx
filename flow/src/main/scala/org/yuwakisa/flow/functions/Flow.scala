package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Cell, Grid}

object Flow:
  def flowNeighbors(g: Grid, x: Int, y: Int): Double = {
    for xx <- math.max(0, x - 1) to math.min(g.lastX, x + 1);
         yy <- math.max(0, y - 1) to math.min(g.lastY, y + 1)
      yield
        val a = xx + g.vx(xx, yy)
        val c = yy + g.vy(xx, yy)

        val sx = if a >= x && a <= x + 1 then x + 1 - a
        else if a + 1 >= x && a <= x then a + 1 - x
        else 0

        val sy = if c >= y && c <= y + 1 then y + 1 - c
        else if c + 1 >= y && c <= y then c + 1 - y
        else 0

        sx * sy * g.values(xx, yy)
  }.sum

  def life(v: Double): Double =
    if v < 0.2 then
      v * 0.5
    else if v > 0.5 then
      v * 0.5
    else
      v * 2

  def identity(v: Double): Double = v

  def flow(c: Double, v: Double): Double = Cell.minMax(v * 0.8 + c * 0.2)

  def haFlow(c: Double, v: Double): Double = Cell.minMax(v * 0.5 + c * 0.5)

  def slowFlow(c: Double, v: Double): Double = Cell.minMax(v * 0.1 + c * 0.9)

  def dimFlow(c: Double, v: Double): Double = Cell.minMax(v * 0.5 + c * 0.47)

  def ampFlow(c: Double, v: Double): Double = Cell.minMax(v * 0.5 + c * 0.55)

  def jump(c: Double, v: Double): Double = Cell.minMax(v)

  def jumpLife(c: Double, v: Double): Double = life(Cell.minMax(v)).toFloat

  def apply(g: Grid): Grid =
    g.newValues({ (x, y) =>
      val c = g.values(x, y)
      val v = flowNeighbors(g, x, y)
      Cell.bounce(slowFlow(c, v)).toFloat
    })
