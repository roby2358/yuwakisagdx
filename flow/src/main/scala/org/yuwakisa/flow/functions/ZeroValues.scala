package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Grid, Layer}

object ZeroValues:
  def apply(g: Layer): Layer = g.newValues({ (_, _) => 0 })

  def apply(g: Grid): Grid = g.copy(values = this (g.values))
