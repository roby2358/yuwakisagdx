package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Cell, Grid, Layer}

import scala.util.Random

object Plasma:

  class Filler(g: Layer, scale: Double):

    def assign(x: Int, y: Int, v: Double): Unit =
      g.cells(y)(x) = Cell.bounce(v).toFloat

    def straightRange(a: Double): Double = math.abs(a) / scale

    def sqrtRange(a: Double): Double = math.sqrt(math.abs(a) / scale)

    def sigmoidRange(a: Double): Double =
      val v = math.abs(a) / scale
      1 / (1 + math.pow(math.E, -v))
    
    val range: Double => Double = straightRange

    def noJiggle(a: Double): Double = 1d

    def randomJiggle(a: Double): Double =
      (Random.nextDouble() - 0.5) * range(a)

    def gaussianJiggle(a: Double): Double =
      Random.nextGaussian() / 3 * range(a)
      
    val jiggle: Double => Double = gaussianJiggle

    def average(x: Int, y: Int, x0: Int, y0: Int, x1: Int, y1: Int): Unit =
      val vv = g(x0, y0) + g(x1, y1)
      val span = math.max(x1 - x0, y0 - y1)
      assign(x, y, vv / 2 + jiggle(span))

    def average4(hx: Int, hy: Int, a: Int, b: Int, c: Int, d: Int): Unit =
      val vv = g(a, b) +
        g(a, d) +
        g(c, b) +
        g(c, d)
      val span = math.max(c - a, d - b)
      assign(hx, hy, vv / 4f + jiggle(span / 2).toFloat)

    def rangeRand: Double = Random.nextDouble() * 0.25f + 0.25f

    def interp(a: Int, b: Int, p: Double): Int = (a * p + b * (1f - p)).toInt

    def fill(x0: Int, y0: Int, x1: Int, y1: Int): Unit =
      if x1 - x0 < 2 && y1 - y0 < 2 then return

      val p = 0.5f
      val hx = interp(x0, x1, p)
      val hy = interp(y0, y1, p)

      if x1 < g.lastX then
        average(x1, hy, x1, y0, x1, y1)
      if y1 < g.lastY then
        average(hx, y1, x0, y1, x1, y1)
      average4(hx, hy, x0, y0, x1, y1)

      fill(x0, y0, hx, hy)
      fill(hx, y0, x1, hy)
      fill(x0, hy, hx, y1)
      fill(hx, hy, x1, y1)

    def setHLine(v: Double, x0: Int, y: Int, x1: Int): Unit =
      (x0 to x1).foreach(x => assign(x, y, v))

    def setVLine(v: Double, x: Int, y0: Int, y1: Int): Unit =
      (y0 to y1).foreach(y => assign(x, y, v))

    def hline(x0: Int, y0: Int, x1: Int): Unit =
      val hx = (x0 + x1) / 2

      average(hx, y0, x0, y0, x1, y0)

      if x0 + 1 < hx then
        hline(x0, y0, hx)

      if hx + 1 < x1 then
        hline(hx, y0, x1)

    def vline(x0: Int, y0: Int, y1: Int): Unit =
      val hy = (y0 + y1) / 2

      average(x0, hy, x0, y0, x0, y1)

      if y0 + 1 < hy then
        vline(x0, y0, hy)

      if hy + 1 < y1 then
        vline(x0, hy, y1)

    def randomStart(): Filler =
      assign(0, 0, scala.util.Random.nextDouble())
      assign(g.lastX, 0, scala.util.Random.nextDouble())
      assign(0, g.lastY, scala.util.Random.nextDouble())
      assign(g.lastX, g.lastY, scala.util.Random.nextDouble())
      hline(0, 0, g.lastX)
      vline(0, 0, g.lastY)
      hline(0, g.lastY, g.lastX)
      vline(g.lastX, 0, g.lastY)
      this

    def pushInStart(): Filler =
      val hy = g.lastY / 2
      setHLine(0.75, 0, 0, g.lastX)
      setVLine(0.5, 0, 0, g.lastY)
      setHLine(0.25, 0, g.lastY, g.lastX)
      setVLine(1d, g.lastX, 0, hy)
      setVLine(0d, g.lastX, hy, g.lastY)
      this

    def swirlStart(): Filler =
      setHLine(0.5, 0, 0, g.lastX)
      setVLine(0.87, g.lastX, 0, g.lastY)
      setHLine(0.12, 0, g.lastY, g.lastX)
      setVLine(0.37, 0, 0, g.lastY)
      this

    def zeroSides(): Filler =
      setHLine(0d, 0, 0, g.lastX)
      setVLine(0d, g.lastX, 0, g.lastY)
      setHLine(0d, 0, g.lastY, g.lastX)
      this

    def apply(): Layer =
      fill(0, 0, g.lastX, g.lastY)
      g

  def plasma(lumpy: Double = 4d)(g: Layer): Layer =
    new Filler(Layer.zero(g.width, g.height), g.width / lumpy)
      .randomStart()
      .apply()
  
  
  def plasmaIsland(lumpy: Double = 8d)(g: Layer): Layer =
    new Filler(Layer.zero(g.width, g.height), g.width / lumpy)
      .zeroSides()
      .apply()

  def grid(g: Grid): Grid = g.copy(values = plasma(4)(g.values))
