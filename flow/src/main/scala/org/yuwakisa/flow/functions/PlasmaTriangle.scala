package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Cell, Layer, Point}

import scala.collection.mutable
import scala.util.Random

object PlasmaTriangle:

  def rr: Float = Cell.minMax(Random.nextDouble * .999 + .001).toFloat

  def plasma(gg: Layer): Layer =
    val a = Point(0, 0)
    val b = Point(gg.lastX, 0)
    val c = Point(gg.lastX, gg.lastY)
    val d = Point(0, gg.lastY)
    val e = a.mid(b)
    val f = a.mid(d)
    val g = d.mid(e)
    val h = c.mid(e)
    val i = b.mid(c)

    val points = mutable.Set(a, b, c, d, e, f, g, h, i)

    val plas = new PlasmaTriangle(gg)

    //    points.foreach(p => {
    //      g.set(p, rr)
    //    })

    gg.set(a, .5f)
    points.foreach(p => {
      //      gg.set(p, plas.avg(p, points.toSet))
      gg.set(p, .5f)
    })

    //        (0 until 1000).foreach(_ => {
    //          val p = Point(Random.nextInt(g.width), Random.nextInt(g.height))
    //          points += p
    //          g.set(p, plas.avg(p, points.toSet))
    //        })

    val sanity = (math.log(gg.width) / math.log(2) + 4).toInt

    plas
      .fill(c, d, e, sanity)
      .fill(e, d, a, sanity)
      .fill(c, d, e, sanity)
      .fill(b, h, e, sanity)
      .fill(a, e, g, sanity)
      .fill(b, e, h, sanity)
      .fill(b, c, h, sanity)
      .fill(a, d, g, sanity)

    gg

class PlasmaTriangle(g: Layer):

  def noVariance(t: Double): Double = 0d

  def straightSqrtVariance(t: Double): Double =
    (Random.nextDouble() * 2d - 1d) * math.sqrt(t / g.width)

  def gaussianStraightVariance(t: Double): Double =
    (Random.nextGaussian() / 6d) * (t / g.width)

  def gaussianSinVariance(t: Double): Double =
    (Random.nextGaussian() / 6d) *
      math.sin(math.Pi * t / g.width / 2d - math.Pi)

  def gaussianSqrtVariance(t: Double): Double =
    (Random.nextGaussian() / 6d) * math.sqrt(t / g.width)

  def gaussianSigmaTimeVariance(t: Double): Double =
    t * Random.nextGaussian() / (g.width * math.sqrt(t))

  def avg(p: Point, points: Set[Point]): Float =
    val others = points
      .filter(q => p != q && g(q) != 0f)
      .toVector
      .map(q => {
        (g(q), p.distance(q))
      })

    if others.isEmpty then return Random.nextFloat

    val weight = others.map({ case (_, d) => 1 / d })
    val weighted = others.map({ case (v, d) => v / d })
    val vari = others.map({ case (_, d) => gaussianSigmaTimeVariance(d) })
      .sum / others.length

    Cell.minMax(weighted.sum / weight.sum + vari).toFloat

  def fill(a: Point, b: Point, c: Point, sanity: Int): PlasmaTriangle =
    if sanity <= 0 then return this

    val ab = a.mid(b)
    val ac = a.mid(c)
    val bc = b.mid(c)

    val points = Set(a, b, c)
    val newPoints = Set(ab, ac, bc)

    if (newPoints -- points).isEmpty then return this

    newPoints
      .filter(p => g(p) == 0f)
      .foreach({ p =>
        g.set(p, avg(p, points))
      })

    fill(ab, ac, bc, sanity - 1)
    fill(a, ab, ac, sanity - 1)
    fill(b, ab, bc, sanity - 1)
    fill(c, ac, bc, sanity - 1)

    this
