package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.Layer

object BubbleNoise:

  def apply(g: Layer): Layer =
    val a = Layer.random(g.width, g.height)
    val b = a << Blur(2).gaussian
    val c = a << Blur(4).gaussian
    val d = a << Blur(8).gaussian
    val e = a << Blur(16).gaussian

    //    a.weightedAdd(Seq(a, b, c, d, e), Seq(1, 1, 1, 1, 1))

    a
      .add(b)
      .add(c)
      .add(d)
      .add(e) <<
      Stretch.hilo
