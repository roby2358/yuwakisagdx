package org.yuwakisa.flow.functions

import org.yuwakisa.flow.model.{Cell, Grid}

object ValueToVector:
  def apply(g: Grid): Grid =
    g.newLayers({ (x, y) =>
      val (vx, vy) = Cell.valueToVector(g.values(x, y))
      (0f, vx.toFloat, vy.toFloat)
    })
