package org.yuwakisa.flow

import com.badlogic.gdx.Input.Buttons
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{Color, GL20, Pixmap, PixmapIO}
import com.badlogic.gdx.utils.{Disposable, ScreenUtils}
import com.badlogic.gdx.{Gdx, InputAdapter}
import org.yuwakisa.flow.Stuff._
import org.yuwakisa.flow.stuff.ScreenCap

import java.io.File
import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId}
import java.util.Locale

object GameView:
  val dateFormatter: DateTimeFormatter = DateTimeFormatter
    .ofPattern("yyyyMMddHHmmss")
    .withLocale(Locale.UK)
    .withZone(ZoneId.systemDefault())

case class GameView(state: GameState):

  import GameView._

  lazy val batch = new SpriteBatch() with Disposable
  lazy val toDispose: Seq[Disposable] = Seq(batch)
  var screenCap = false

  lazy val startInputter: Boolean =
    val inputter = new InputAdapter() {
      override def touchDown(x: Int, y: Int, pointer: Int, button: Int)
      : Boolean =
        if button == Buttons.RIGHT then onRightClick(x, y)
        false
    }

    Gdx.input.setInputProcessor(inputter)
    true

  /**
   * Build graphics configuration
   *
   * @return
   */
  def config: LwjglApplicationConfiguration =
    (new LwjglApplicationConfiguration).tap({ cfg =>
      cfg.width = state.width
      cfg.height = state.height

      cfg.vSyncEnabled = false
      cfg.foregroundFPS = 0
      cfg.backgroundFPS = 0
    })

  /**
   * Create the view
   */
  def create(): Unit =
    startInputter

  /**
   * Dispose the things that need to be cleaned up
   */
  def dispose(): Unit = toDispose.foreach(_.dispose())

  /**
   * Prepare to render!
   */
  def prepRender(): Unit =
//    val c = Color.BLACK
//    Gdx.gl.glClearColor(c.r, c.g, c.b, 1)
//    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    Gdx.gl.glEnable(GL20.GL_BLEND)
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
    Gdx.gl.glDisable(GL20.GL_BLEND)

  def checkScreenCap() =
    if screenCap then
      state.screencap()
      screenCap = false

  /**
   * Handle a click
   *
   * @param x mouse click x
   * @param y mouse click y
   */
  def onRightClick(x: Int, y: Int): Unit =
    screenCap = true

  /**
   * Render the window
   */
  def render(): Unit =
    prepRender()
    state.render(batch)
    checkScreenCap()


