package com.yuwakisa.tank.desktop

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl.{LwjglApplication, LwjglApplicationConfiguration}
import com.yuwakisa.tank.TankGame

object DesktopLauncher {
  def main(args: Array[String]): Unit = {

    lazy val width = 1200 // Gdx.graphics.getDisplayMode.width

    lazy val height = 800 // Gdx.graphics.getDisplayMode.height

    /**
     * Build graphics configuration
     *
     * @return
     */
    def config: LwjglApplicationConfiguration = {
      val c = new LwjglApplicationConfiguration
      c.width = width
      c.height = height
      c
    }

    /**
     * Create the view
     */
    def create(): Unit = {
      (width, height)
    }

    new LwjglApplication(new TankGame(width, height), config)
  }
}
