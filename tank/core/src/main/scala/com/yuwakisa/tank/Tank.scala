package com.yuwakisa.tank

import breeze.linalg.DenseMatrix
import com.badlogic.gdx.graphics.Color

object Tank {

  val points = DenseMatrix(
    (-.25f, -.4f, 1f),
    (-.25f, .3f, 1f),
    (-.15f, .3f, 1f),
    (-.15f, .1f, 1f),
    (-.05f, .1f, 1f),
    (-.05f, .4f, 1f),
    (.05f, .4f, 1f),
    (.05f, .1f, 1f),
    (.15f, .1f, 1f),
    (.15f, .3f, 1f),
    (.25f, .3f, 1f),
    (.25f, -.4f, 1f),
    (.15f, -.4f, 1f),
    (.15f, -.3f, 1f),
    (-0.15f, -.3f, 1f),
    (-0.15f, -.4f, 1f),
    (-0.25f, -.4f, 1f))
}

case class Tank(var x: Int, var y: Int, var a: Double, color: Color)
