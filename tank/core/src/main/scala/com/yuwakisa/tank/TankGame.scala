package com.yuwakisa.tank

import breeze.linalg.DenseMatrix
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import com.badlogic.gdx.graphics.{Color, GL20}
import com.badlogic.gdx.{ApplicationAdapter, Gdx}

class TankGame(width: Int, height: Int) extends ApplicationAdapter {
  lazy val batch: SpriteBatch = new SpriteBatch()
  lazy val shapeRenderer: ShapeRenderer = new ShapeRenderer()

  var tank = Tank(1, 1, 0, Color.BLACK)

  override def create(): Unit = {}

  override def render(): Unit = {
    Gdx.gl.glClearColor(0, 0.5f, 0.2f, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    batch.begin()
    shapeRenderer.begin(ShapeType.Line)
    shapeRenderer.setColor(Color.BLACK)

    tank.a -= math.Pi / 10d
    tank.x += 1
    tank.y += 1

    val ww = width * 2
    val hh = height * 2
    var x = (tank.x % ww)
    var y = (tank.y % hh)
    if (x > width) x = ww - x
    if (x < 0) x += width
    if (y > height) y = hh - y
    if (y < 0) y += height

    val r = DenseMatrix(
      (math.cos(tank.a).toFloat, math.sin(tank.a).toFloat, 0f),
      (-math.sin(tank.a).toFloat, math.cos(tank.a).toFloat, 0f),
      (0f, 0f, 1f))

    val t = DenseMatrix(
      (1f, 0f, 0f),
      (0f, 1f, 0f),
      (x.toFloat, y.toFloat, 1f))

    val s = DenseMatrix(
      (20f, 0f, 0f),
      (0f, 20f, 0f),
      (0f, 0f, 1f))

    val p = Tank.points * s * r * t

    (1 until Tank.points.rows).foreach({ i =>
      val a = p(i - 1, 0)
      val b = p(i - 1, 1)
      val c = p(i, 0)
      val d = p(i, 1)
      shapeRenderer.line(a, b, c, d)
    })

    shapeRenderer.end()
    batch.end()
  }

  override def dispose(): Unit = {
    batch.dispose()
    shapeRenderer.dispose()
  }
}
