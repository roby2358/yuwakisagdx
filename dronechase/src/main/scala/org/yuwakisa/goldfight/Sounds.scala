package org.yuwakisa.goldfight

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.audio.Sound

object Sounds {
  val gold: Seq[Sound] = Seq(
    "Bag-of-coins-D-www.fesliyanstudios.com.mp3",
    "Catching-Coins-In-Hand-B-www.fesliyanstudios.com.mp3",
    "Coins-Falling-on-floor-a-www.fesliyanstudios.com.mp3",
    "Collecting-Money-Coins-A-www.fesliyanstudios.com.mp3",
    "Collecting-Money-Coins-E-www.fesliyanstudios.com.mp3",
    "Collecting-Money-Coins-G-www.fesliyanstudios.com.mp3",
    "Handling-Coins-A-www.fesliyanstudios.com.mp3"
  )
    .map { n => Gdx.audio.newSound(Gdx.files.internal(n)) }

  val swords: Seq[Sound] = Seq(
    "Swords Clashing-SoundBible.com-912903192.mp3",
    "Swords_Collide-Sound_Explorer-2015600826.mp3",
    "saber_6.mp3",
    "SF-hit_armour_3.mp3"
  )
    .map { n => Gdx.audio.newSound(Gdx.files.internal(n)) }
}
