package org.yuwakisa.goldfight

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType
import org.yuwakisa.goldfight.GameView.Colors

case class ViewHelpers(gameView: GameView) {

  lazy val font = new BitmapFont()

  val haPix = gameView.gameState.grid.haPix

  def showDistances(from: Int): Unit =
    (0 to gameView.grid.width * gameView.grid.height).foreach({ to =>
      val (ax, ay) = gameView.grid.pixXY(to)
      val d = gameView.grid.distance(from, to)

      if (true) {
        showCell(to, Colors(d % Colors.size), haPix)
      }
      else {
        font.setColor(Color.WHITE)
        // hmmmm
        //        val layout = new GlyphLayout
        //        layout.setText(font, d.toString)
        //        println(layout.width, layout.height)
        font.draw(gameView.batch, d.toString, ax + 11, ay + 22)
      }
    })

  def showRow(at: Int): Unit = {
    val (start, end) = gameView.grid.atRow(at)
    (start until end).foreach { a =>
      val (x, y) = gameView.grid.pixXY(a)
      gameView.shapeRenderer.setColor(Color.NAVY)
      gameView.shapeRenderer.begin(ShapeType.Filled)
      gameView.shapeRenderer.circle(x + gameView.grid.haPix, y + gameView.grid.haPix, haPix)
      gameView.shapeRenderer.end()
    }
  }

  def showCell(at: Int, color: Color, radius: Int): Unit = {
    val (x, y) = gameView.grid.pixXY(at)
    gameView.shapeRenderer.setColor(color)
    gameView.shapeRenderer.begin(ShapeType.Filled)
    gameView.shapeRenderer.circle(x + haPix, y + haPix, radius)
    gameView.shapeRenderer.end()
  }

  def showCells(ats: Seq[Int]): Unit =
    ats.foreach(showCell(_, Color.NAVY, haPix))

  def showNextStep(at: Int): Unit = {
    val target = gameView.grid.lastAt / 2 - gameView.grid.width / 2
    val to = gameView.grid.nextStep(at, target)

    showCell(target, Color.RED, haPix)
    showCell(to, Color.YELLOW, haPix)
  }

  def showMouseCell(mouseAt: Int): Unit =
    showCell(mouseAt, Color.WHITE, haPix)

}
