package org.yuwakisa.goldfight

case object GoldFightAnimations {

  val TakeTreasure: Animation = Animation(Seq(
    AnimationStep(25, 200),
    AnimationStep(26, 200),
    AnimationStep(27, 200),
    AnimationStep(28, 200),
    AnimationStep(29, 500)
  ))

  val Vanquish: Animation = Animation(Seq(
    AnimationStep(30, 100),
    AnimationStep(31, 200),
    AnimationStep(32, 300)
  ))

  val Attack: Animation = Animation(Seq(
    AnimationStep(33, 400)
  ))

  val Defend: Animation = Animation(Seq(
    AnimationStep(34, 400)
  ))
}
