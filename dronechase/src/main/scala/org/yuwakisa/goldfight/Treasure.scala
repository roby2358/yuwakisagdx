package org.yuwakisa.goldfight

case class Treasure(gold: Int) extends Thing {

  def image: Int = math.min(4, gold / 100)
}
