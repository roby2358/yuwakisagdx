package org.yuwakisa.goldfight

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration

class GoldFight extends ApplicationAdapter {
  val animations = Animations()
  val gameState: GameState = GameStart(animations).begin()
  val gameView: GameView = GameView(gameState, animations)

  def config: LwjglApplicationConfiguration = gameView.config

  override def create(): Unit = gameView.create()

  override def render(): Unit = {
    gameState.timedGo()
    gameView.render()
  }

  override def dispose(): Unit = gameView.dispose()
}
