package org.yuwakisa

package object goldfight {

  implicit class Doer[A](a: A) {

    def tap[B](f: => B): A = {
      f
      a
    }

    def tapWith[B](f: A => B): A = {
      f(a)
      a
    }
  }
}
