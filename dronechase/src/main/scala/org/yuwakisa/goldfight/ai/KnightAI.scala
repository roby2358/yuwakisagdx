package org.yuwakisa.goldfight.ai

import org.yuwakisa.goldfight.{GameState, Plan, ThingAt}

trait KnightAI {

  def plan(gameState: GameState, ta: ThingAt): Plan
}
