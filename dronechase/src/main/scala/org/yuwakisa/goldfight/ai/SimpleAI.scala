package org.yuwakisa.goldfight.ai

import org.yuwakisa.goldfight._

case object SimpleAI extends KnightAI {

  case class Best(at: Int, distance: Int)

  def findClosest(gameState: GameState, at: Int): Option[Int] = {
    if (gameState.things.length == 1)
      return None

    Some(gameState.things
      .filter(_.at != at)
      .minBy { ta =>
        gameState.grid.distance(at, ta.at)
      }
      .at)
  }

  def plan(gameState: GameState, ta: ThingAt): Plan = {
    val target = findClosest(gameState, ta.at)
    target match {
      case Some(b) =>
        Move(ta.at, gameState.grid.nextStep(ta.at, b))
      case None =>
        Move(ta.at, ta.at)
    }
  }
}
