package org.yuwakisa.goldfight

import com.badlogic.gdx.audio.Sound

import scala.util.Random

object Events {

  sealed trait Event {
    def at: Int
    def animation: Animation
    def sound: Option[Sound]
  }

  trait EventSink {
    def take(event: Event): Unit = {}
  }

  case class TakeTreasure(at: Int) extends Event {
    val animation: Animation = GoldFightAnimations.TakeTreasure
    val sound: Option[Sound] = Some(Sounds.gold(Random.nextInt(Sounds.gold.length)))
  }

  case class Attack(at: Int) extends Event {
    val animation: Animation = GoldFightAnimations.Attack
    val sound: Option[Sound] = Some(Sounds.swords(Random.nextInt(Sounds.swords.length)))
  }

  case class Defend(at: Int) extends Event {
    val animation: Animation = GoldFightAnimations.Defend
    val sound: Option[Sound] = None
  }

  case class Vanquish(at: Int) extends Event {
    val animation: Animation = GoldFightAnimations.Vanquish
    val sound: Option[Sound] = Some(Sounds.swords(Random.nextInt(Sounds.swords.length)))
  }

}
