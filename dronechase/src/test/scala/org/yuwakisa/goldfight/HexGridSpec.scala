package org.yuwakisa.goldfight

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class HexGridSpec extends AnyFlatSpec with Matchers {

  private val grid = HexGrid(20, 10, 32)

  "pixXY" must "pick the correct X Y" in {
    grid.pixXY(0) must be (0, 0)
    grid.pixXY(20) mustBe (16, 32)
    grid.pixXY(1) mustBe (32, 0)
    grid.pixXY(19) mustBe (608, 0)
    grid.pixXY(20 * 10 + 19) mustBe (608, 320)
    grid.pixXY(20 * 10) mustBe (0, 320)
  }

  "shifedRow" must "identify the correct shifted row" in {
    grid.shiftedRow(0) mustBe 0
    grid.shiftedRow(20) mustBe 1
  }
}
