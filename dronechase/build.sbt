name := "dronechase"

version := "0.1"

scalaVersion := "2.13.3"

resolvers ++= Seq(
  "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/",
  "Sonatype Releases" at "https://oss.sonatype.org/content/repositories/releases/"
)

libraryDependencies ++= Seq(
  "com.badlogicgames.gdx" % "gdx" % "1.9.11",
  "com.badlogicgames.gdx" % "gdx-box2d" % "1.9.11",
  "com.badlogicgames.gdx" % "gdx-controllers" % "1.9.11",

  "com.badlogicgames.gdx" % "gdx-backend-lwjgl" % "1.9.11",
  "com.badlogicgames.gdx" % "gdx-platform" % "1.9.11" classifier "natives-desktop",

  "org.scalactic" %% "scalactic" % "3.2.0",
  "org.scalatest" %% "scalatest" % "3.2.0" % "test"

//  "com.badlogicgames.gdx" % "gdx-ai" % "1.9.11"
)