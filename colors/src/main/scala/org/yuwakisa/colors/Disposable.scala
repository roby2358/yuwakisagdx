package org.yuwakisa.colors

trait Disposable {

  def dispose(): Unit
}
