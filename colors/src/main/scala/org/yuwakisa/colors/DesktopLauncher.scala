package org.yuwakisa.colors

import com.badlogic.gdx.backends.lwjgl.LwjglApplication

object DesktopLauncher {

  def main(arg: Array[String]): Unit = {
    val game = new org.yuwakisa.colors.Main
    new LwjglApplication(game, game.config)
  }
}