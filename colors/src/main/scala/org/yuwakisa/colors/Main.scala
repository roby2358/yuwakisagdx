package org.yuwakisa.colors

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration

class Main extends ApplicationAdapter {
  val state: GameState = GameState()
  val view: GameView = GameView(state)

  def config: LwjglApplicationConfiguration = view.config

  override def create(): Unit = view.create()

  override def render(): Unit = {
    state.timedGo()
    view.render()
  }

  override def dispose(): Unit = view.dispose()
}
