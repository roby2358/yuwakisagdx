package org.yuwakisa

package object colors {

  implicit class Doer[A](a: A) {

    def tap[B](f: => B): A = {
      f
      a
    }

    def tapWith[B](f: A => B): A = {
      f(a)
      a
    }

    def chain[B](f: A => B): B = f(a)
  }

  def sqr(n: Double): Double = n * n
}
