package org.yuwakisa.colors

import org.yuwakisa.colors.ColorWheel.Rgb

object Luminance {

  def w3c(rgb: Rgb): Double = rgb._1 * .299 + rgb._2 * .587 + rgb._3 * .117

  def srgb(rgb: Rgb): Double = rgb._1 * .2126 + rgb._2 * .7152 + rgb._3 * .0722

  def euclidian(rgb: Rgb): Double = math.sqrt(rgb._1 * rgb._1 * .299 +
    rgb._2 * rgb._2 * .587 +
    rgb._3 * rgb._3 * .114)
}
