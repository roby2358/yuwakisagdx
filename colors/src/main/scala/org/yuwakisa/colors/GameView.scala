package org.yuwakisa.colors

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.SpriteBatch

trait Disposable {
  def dispose(): Unit
}

case class GameView(state: GameState) {

  lazy val batch = new SpriteBatch()
  lazy val toDispose: Seq[Disposable] = Seq()

  /**
   * Build graphics configuration
   *
   * @return
   */
  def config: LwjglApplicationConfiguration =
    (new LwjglApplicationConfiguration).tapWith({ cfg =>
      cfg.width = state.width
      cfg.height = state.height
    })

  /**
   * Create the view
   */
  def create(): Unit = {
    println(Gdx.graphics.getDisplayMode.width, Gdx.graphics.getDisplayMode.height)
  }

  /**
   * Dispose the things that need to be cleaned up
   */
  def dispose(): Unit = toDispose.foreach(_.dispose())

  /**
   * Prepare to render!
   */
  def prepRender(): Unit = {
    Gdx.gl.glClearColor(0, 0, 0, 1)
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

    Gdx.gl.glEnable(GL20.GL_BLEND)
    Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)
    Gdx.gl.glDisable(GL20.GL_BLEND)
  }

  /**
   * Render the window
   */
  def render(): Unit = {
    prepRender()
    state.render(batch)
  }
}
