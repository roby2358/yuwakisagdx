package org.yuwakisa.colors.schemes

import org.yuwakisa.colors.ColorWheel.Rgb
import org.yuwakisa.colors.{ColorWheel, Luminance, Radial, _}

import scala.util.Random

case class Schemes(wheel: ColorWheel) {

  def OneSpoke: Double = 1d / 12d

  def randomR: Double = 0.8 - Random.nextDouble * 0.8

  def lumsort(s: IndexedSeq[Rgb]): IndexedSeq[Rgb] = s.sortWith((a, b) =>
    Luminance.euclidian(a) > Luminance.euclidian(b))

  def mod1(a: Double): Double =
    if (a < 0.0) a + 1.0
    else if (a > 1.0) a - 1.0
    else a

  def monochromatic(rr: Radial): IndexedSeq[Rgb] =
    Seq.fill(5)(randomR)
      .sorted
      .toIndexedSeq
      .map(Radial(rr.a, _))
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)

  def analogous(rr: Radial): IndexedSeq[Rgb] =
    IndexedSeq(
      Radial(rr.a, rr.r),
      Radial(rr.a, randomR),
      Radial(mod1(rr.a + OneSpoke), rr.r),
      Radial(mod1(rr.a + OneSpoke), randomR),
      Radial(mod1(rr.a - OneSpoke), rr.r)
    )
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)


  def triad(rr: Radial): IndexedSeq[Rgb] =
    IndexedSeq(
      Radial(rr.a, rr.r),
      Radial(rr.a, randomR),
      Radial(mod1(rr.a + 4 * OneSpoke), rr.r),
      Radial(mod1(rr.a + 4 * OneSpoke), randomR),
      Radial(mod1(rr.a - 4 * OneSpoke), rr.r)
    )
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)

  def complementary(rr: Radial): IndexedSeq[Rgb] =
    IndexedSeq(
      Radial(rr.a, rr.r),
      Radial(rr.a, mod1(rr.r - 0.1d)),
      Radial(rr.a, mod1(rr.r + 0.1d)),
      Radial(mod1(rr.a + 6 * OneSpoke), rr.r),
      Radial(mod1(rr.a + 6 * OneSpoke), randomR)
    )
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)

  def splitComplementary(rr: Radial): IndexedSeq[Rgb] =
    IndexedSeq(
      Radial(rr.a, rr.r),
      Radial(mod1(rr.a + 5 * OneSpoke), rr.r),
      Radial(mod1(rr.a + 5 * OneSpoke), randomR),
      Radial(mod1(rr.a - 5 * OneSpoke), rr.r),
      Radial(mod1(rr.a - 5 * OneSpoke), randomR),
    )
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)

  def doubleSplitComplementary(rr: Radial): IndexedSeq[Rgb] =
    IndexedSeq(
      Radial(rr.a, rr.r),
      Radial(mod1(rr.a + 1 * OneSpoke), rr.r),
      Radial(mod1(rr.a - 1 * OneSpoke), rr.r),
      Radial(mod1(rr.a + 5 * OneSpoke), rr.r),
      Radial(mod1(rr.a - 5 * OneSpoke), rr.r),
    )
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)

  def square(rr: Radial): IndexedSeq[Rgb] =
    IndexedSeq(
      Radial(rr.a, rr.r),
      Radial(rr.a, randomR),
      Radial(mod1(rr.a + 4 * OneSpoke), rr.r),
      Radial(mod1(rr.a - 4 * OneSpoke), rr.r),
      Radial(mod1(rr.a + 6 * OneSpoke), rr.r),
    )
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)

  def compound(rr: Radial): IndexedSeq[Rgb] =
    IndexedSeq(
      Radial(rr.a, rr.r),
      Radial(mod1(rr.a - 1 * OneSpoke), rr.r),
      Radial(mod1(rr.a - 1 * OneSpoke), randomR),
      Radial(mod1(rr.a - 5 * OneSpoke), rr.r),
      Radial(mod1(rr.a - 5  * OneSpoke), randomR),
    )
      .map(wheel.pixToRGBLuminosity)
      .chain(lumsort)
}
