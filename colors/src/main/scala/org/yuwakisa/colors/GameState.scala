package org.yuwakisa.colors

import com.badlogic.gdx.Input.Buttons
import com.badlogic.gdx.graphics.Pixmap.Format
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.{Color, Pixmap, Texture}
import com.badlogic.gdx.{Gdx, InputAdapter}

case class GameState() extends Disposable {

  val height: Int = 800
  val width: Int = 1000
  val radius: Int = math.min(height / 2, width / 2)
  lazy val pixmap = new Pixmap(width, height, Format.RGB888)
  val wheel: ColorWheel = ColorWheel(height / 2)
  val schemes: Schemes = Schemes(wheel)

  val WithoutColor: Color = Color.BLACK

  val WithoutScheme = Seq()

  var previewColor: Color = WithoutColor

  var selectedColor: Color = WithoutColor

  var selectedScheme: Seq[Color] = WithoutScheme

  lazy val woo: Boolean = {
    val inputter = new InputAdapter() {
      override def mouseMoved(x: Int, y: Int): Boolean = {
        preview(x, y)
        false
      }

      override def touchDown(x: Int, y: Int, pointer: Int, button: Int)
      : Boolean = {
        if (button == Buttons.LEFT) select(x, y)
        false
      }
    }

    Gdx.input.setInputProcessor(inputter)
    true
  }

  def toRadial(x: Int, y: Int): Option[Radial] = {
    val r = math.sqrt(sqr(x - radius) + sqr(y - radius))

    if (r > radius) return None

    val a = 0.5 + math.atan2(radius - y, radius - x) / (2 * math.Pi)

    Some(Radial(a, r / radius))
  }

  def toColor(rgb: ColorWheel.Rgb) = new Color(rgb._1.toFloat, rgb._2.toFloat,
    rgb._3.toFloat, 1f)

  def colorAt(x: Int, y: Int): Color =
    toRadial(x, y)
      .map(wheel.pixToRGBLuminosity)
      .map(toColor)
      .getOrElse(WithoutColor)

  def preview(x: Int, y: Int): Unit = {
    previewColor = colorAt(x, y)
  }

  def select(x: Int, y: Int): Unit = {
    selectedColor = colorAt(x, y)
    selectedScheme = toRadial(x, y)
      .map(schemes.analogous)
      .getOrElse(WithoutScheme)
      .map(toColor)
  }

  def timedGo(): Unit = {}

  def render(batch: SpriteBatch): Unit = {
    woo

    for (y <- 0 until height;
         x <- 0 until width) {
      pixmap.setColor(colorAt(x, y))
      pixmap.drawPixel(x, y)
    }

    pixmap.setColor(previewColor)
    pixmap.fillRectangle(width - 100, 0, 100, 50)

    pixmap.setColor(selectedColor)
    pixmap.fillRectangle(width - 100, 50, 100, 50)

    for (i <- selectedScheme.indices) {
      pixmap.setColor(selectedScheme(i))
      pixmap.fillRectangle(width - 100, 100 + i * 50 + 50, 100, 50)
    }

    val text = new Texture(pixmap)
    batch.begin()
    batch.draw(text, 0, 0)
    batch.end()
    text.dispose()
  }

  def dispose(): Unit = {
    pixmap.dispose()
  }
}
