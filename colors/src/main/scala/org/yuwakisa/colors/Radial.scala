package org.yuwakisa.colors

import scala.util.Random

object Radial {

  def random: Radial = Radial(Random.nextDouble(), Random.nextDouble())
}

case class Radial(a: Double, r: Double)

