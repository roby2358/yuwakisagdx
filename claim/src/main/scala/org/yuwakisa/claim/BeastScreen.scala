package org.yuwakisa.claim

import com.badlogic.gdx.assets.{AssetDescriptor, AssetManager}
import com.badlogic.gdx.graphics.{GL20, Texture}
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.{FitViewport, Viewport}
import com.badlogic.gdx.{Game, Gdx, Screen}

import scala.util.Random

class BeastScreen(val game: Game)extends Screen :
  val width = 1000
  val height = 800
  val boundary = Rectangle(16, 16, width - 16, height - 16)

  lazy val viewport: FitViewport = FitViewport(width.toFloat, height.toFloat)
  lazy val stage: BeastStage = BeastStage(viewport)

  lazy val beastPic = AssetDescriptor[Texture]("beast.png", classOf[Texture])
  val manager = AssetManager()

  def loadAssets(assets: AssetDescriptor[_]*): Unit =
    assets.foreach(manager.load(_))
    manager.finishLoading()

  override def show(): Unit =
    Gdx.input.setInputProcessor(stage)
    loadAssets(beastPic)
    val beasts: Seq[Beast] = Seq.fill(100)(
      Beast(manager.get(beastPic), boundary)
        .tapWith({ b =>
          b.moveTo(Random.nextFloat * width, Random.nextFloat * height)
          stage.addActor(b)
        }))

  override def render(delta: Float): Unit =
    Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)
    stage.act(delta)
    stage.draw()

  override def resize(width: Int, height: Int): Unit =
    viewport.update(width, height, true)

  override def pause(): Unit = {}

  override def resume(): Unit = {}

  override def hide(): Unit = dispose()

  override def dispose(): Unit = stage.dispose()
