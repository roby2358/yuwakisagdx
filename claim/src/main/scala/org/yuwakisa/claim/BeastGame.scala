package org.yuwakisa.claim

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.badlogic.gdx.graphics.g2d.{Batch, SpriteBatch}
import com.badlogic.gdx.{Application, Game, Gdx}

class BeastGame extends Game :
  val config: LwjglApplicationConfiguration = LwjglApplicationConfiguration()

  override def create(): Unit =
    Gdx.app.setLogLevel(Application.LOG_DEBUG)
    setScreen(new BeastScreen(this))
