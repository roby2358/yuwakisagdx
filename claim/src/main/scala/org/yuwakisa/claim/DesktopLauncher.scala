package org.yuwakisa.claim

import com.badlogic.gdx.backends.lwjgl.LwjglApplication

@main def go(): Unit =
  val game: BeastGame = BeastGame()
  new LwjglApplication(game, game.config)
