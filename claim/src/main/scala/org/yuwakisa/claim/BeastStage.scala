package org.yuwakisa.claim

import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.Viewport
import org.yuwakisa.claim.Beast.Dead

class BeastStage(viewport: Viewport) extends Stage(viewport) :

  def checkCollision(a: Beast, b: Beast): Unit = {
    if a.state == Dead || b.state == Dead then return
    
    if a.box.overlaps(b.box) then
      a.state = Dead
      b.state = Dead
  }

  def beast(i: Int): Beast = getActors.get(i).asInstanceOf[Beast]

  override def act(delta: Float): Unit =
    super.act(delta)
    
    for ( i <- 0 until this.getActors.size ;
          j <- i + 1 until this.getActors.size )
    do
        checkCollision(beast(i), beast(j))
