package org.yuwakisa.claim

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.{Batch, Sprite}
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.{Actor, InputEvent, InputListener, Touchable}

import scala.util.Random

object Beast :
  trait State

  case object Alive extends State

  case object Dead extends State

class Beast(texture: Texture, bounds: Rectangle)extends Actor :
  import Beast._
  
  val sprite = new Sprite(texture)
  var state: State = Alive

  def box: Rectangle = sprite.getBoundingRectangle
  
  def moveTo(x: Float, y: Float): Unit =
    sprite.setPosition(x, y)
    setBounds(sprite.getX, sprite.getY, sprite.getWidth, sprite.getHeight)

  override def act(delta: Float): Unit =
    if state == Dead then return
    
    val nx = sprite.getX + (Random.nextGaussian.toFloat / 3) * delta * 300
    val ny = sprite.getY + (Random.nextGaussian.toFloat / 3) * delta * 300
    moveTo(nx, ny)
    
    if ! bounds.contains(nx, ny) then state = Dead
    
    super.act(delta)
  
  override def draw(batch: Batch, parentAlpha: Float): Unit =
    if state == Alive then sprite.draw(batch)

  val listener: InputListener = new InputListener() :
    override def touchDown(event: InputEvent, x: Float, y: Float, pointer: Int,
                           button: Int): Boolean =
      Gdx.app.log("Touch down asset with name ", "woo")
      true

  addListener(listener)
  setTouchable(Touchable.enabled)
