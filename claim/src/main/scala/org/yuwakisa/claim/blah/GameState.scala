package org.yuwakisa.claim.blah

import com.badlogic.gdx.graphics.g2d.SpriteBatch

trait GameState :

  val height: Int
  
  val width: Int
  
  def next(): Unit = {}
  
  def render(batch: SpriteBatch): Unit
