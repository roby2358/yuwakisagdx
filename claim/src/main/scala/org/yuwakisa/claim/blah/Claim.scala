package org.yuwakisa.claim.blah

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration

class Claim extends ApplicationAdapter :
  val claz = Plain
  val state: GameState = claz.begin()
  val view: GameView = GameView(state)

  def config: LwjglApplicationConfiguration = view.config

  override def create(): Unit = view.create()

  override def render(): Unit =
    state.next()
    view.render()

  override def dispose(): Unit = view.dispose()
