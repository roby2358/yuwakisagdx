package org.yuwakisa.claim.blah

import com.badlogic.gdx.graphics.g2d.SpriteBatch

object Plain:

  val width = 800
  val height = 800

  def begin(): GameState = Plain(width, height)

case class Plain(width: Int, height: Int)extends GameState :

  def render(batch: SpriteBatch): Unit = {}
